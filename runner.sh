#!/bin/bash

set -e

if [ $# -eq 0 ]; then
    cat .gitlab-ci.yml | yq -r '. | to_entries[] | select(.value.stage != null) | .key' | cat -n
    echo "which one ?"
    read idx
else
    idx=$1
fi

target=$(cat .gitlab-ci.yml | yq -r '[. | to_entries[] | select(.value.stage != null) | .key]'"[$idx-1]")

echo "running $target"
set -x

job() {
    target=$1
    # SOURCE VAR from GITLAB
    source <(cat .gitlab-ci.yml | yq -r '.variables | to_entries[] | "export " + .key + "=" + (.value | tostring)')
    source <(cat .gitlab-ci.yml | yq -r '.["'"$target"'"].variables | to_entries[] | "export " + .key + "=" + (.value | tostring)')

    # CUSTOM
    export NEO4J_VERSION=4.1.3-1
    unset DOCKER_HOST
    export CI_COMMIT_SHORT_SHA=local

    # EXEC SCRIPT
    source <(cat .gitlab-ci.yml | yq -r '.["'"$target"'"].script[]')
}

job $target

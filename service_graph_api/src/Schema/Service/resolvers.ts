import db from "../../db";

import debug from "debug";
const logger = debug("SCHEMA:SERVICE");

type DbScreenshots = {
  identity: {
    low: number;
    high: number;
  };
  properties: {
    comment: String;
    contentUrl: String;
    uri: String;
  };
};

type DbService = {
  identity: {
    low: number;
    high: number;
  };
  labels: string[];
  properties: {
    name: string;
    description: string;
    tag: string[];
    version: string;
    marketingImage: string;
    apiType: string;
  };
  screenshots: DbScreenshots[];
};

const fetchAdditionalProperties = async (
  service: DbService
): Promise<DbService> => {
  const id = service.identity.low;
  const screenshots = (
    await db
      .getSession()
      .run(
        `MATCH (service) - [:screenshot] -> (screenshot) WHERE ID(service) = ${id} RETURN screenshot`
      )
  ).records.map((record) => record.get("screenshot"));

  return {
    ...service,
    screenshots,
  };
};
const transformScreenshotToJs = (node: DbScreenshots) => {
  try {
    if (!node.properties.contentUrl && node.properties.uri.startsWith("bnode"))
      return null;
    return {
      contentUrl: node.properties.contentUrl || node.properties.uri,
      comment: node.properties.comment,
    };
  } catch (e) {
    return null;
  }
};

const transformServiceToJs = (node: DbService) => {
  try {
    return {
      id: node.identity.low,
      name: node.properties.name,
      apiType: node.properties.apiType,
      description: node.properties.description,
      marketingImage: node.properties.marketingImage,
      tags: node.properties.tag,
      screenshots: node.screenshots
        .map(transformScreenshotToJs)
        .filter((s) => s !== null),
    };
  } catch (e) {
    console.error(e);
    return null;
  }
};

export const filterdServices = (transform) => (parent, args) => {
  return services(parent, transform(parent, args));
};

export async function services(_parent, args) {
  let query = `MATCH (service:Service) RETURN DISTINCT service`;

  const limitBy = `MATCH ${Object.keys(args)
    .map((relation) =>
      relation.includes("_reverse")
        ? `(service:Service) <- [:${relation.replace(
            "_reverse",
            ""
          )}] - (${relation})`
        : `(service:Service) - [:${relation}] -> (${relation})`
    )
    .join(", ")}
    WHERE 
    ${Object.entries(args)
      .map(
        ([relation, values]: [string, string[]]) =>
          `${relation}.name IN [${values
            .map((value) => `"${value}"`)
            .join(" , ")}]`
      )
      .join(" AND ")}
RETURN DISTINCT service`;

  if (Object.keys(args).length > 0) {
    query = limitBy;
  }
  logger(`Fetching Services with Query: ${query}`);

  const result = await Promise.all(
    (await db.getSession().run(query)).records
      .map((record) => record.get("service"))
      .map(fetchAdditionalProperties)
  );
  return result.map(transformServiceToJs);
}

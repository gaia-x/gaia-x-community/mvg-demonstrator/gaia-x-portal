import { makeExecutableSchema } from "apollo-server";
import merge from "lodash.merge";
import {
  getTypeDef as Provider,
  resolvers as providerResolvers,
} from "./Provider";
import { getTypeDef as Service, resolvers as serviceResolver } from "./Service";
import {
  typeDef as Selectors,
  resolvers as selectorResolver,
} from "./Selectors";

const Query = `
  type Query {
    _empty: String
  }
`;
const resolvers = {};

export default async function generateSchema() {
  return makeExecutableSchema({
    typeDefs: [Query, await Provider(), await Service(), Selectors],
    resolvers: merge(
      resolvers,
      providerResolvers,
      serviceResolver,
      selectorResolver
    ),
  });
}

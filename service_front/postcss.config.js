//preload urls throug issues from nextjs with custom fonts
//https://github.com/vercel/next.js/issues/512#issuecomment-337040792

module.exports = {
  plugins: [
    ["postcss-easy-import", { prefix: "_" }], // keep this first
    ["postcss-url", { url: "inline" }],
    "autoprefixer",
  ],
};

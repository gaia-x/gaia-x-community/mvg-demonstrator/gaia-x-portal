import create from "zustand";

//@ts-ignore
// import { getAll, getSelectors, search } from "@gaia-x/selfdescription";

export const useSelectors = create((set) => ({
  selectors: [],
  fetching: false,
  fetch: async (limitBy = "") => {
    set({ fetching: true });
    let selectors = [];
    try {
      // selectors = await getSelectors(limitBy);
    } catch (e) {
      console.error(e);
    }
    set({ selectors, fetching: false });
  },
}));

export const useSearch = create((set) => ({
  query: {},
  search: async (query) => {
    set({
      query,
    });
  },
}));

import { ApolloClient, InMemoryCache } from "@apollo/client";

export default function createApolloClient(initialState, ctx) {
  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  return new ApolloClient({
    ssrMode: Boolean(ctx),
    uri: process.env.NEXT_PUBLIC_SERVICE_GRAPH_API,
    cache: new InMemoryCache(),
  });
}

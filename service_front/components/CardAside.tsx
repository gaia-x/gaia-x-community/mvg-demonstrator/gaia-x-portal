import React from "react";
import { any, string, bool } from "prop-types";
import { makeStyles } from "./index";
import clsx from "clsx";
const tree = "/images/gaia-x-tree.svg";

const useStyles = makeStyles((theme: any) => ({
  thumb: {
    background: ({ success }: any) =>
      success
        ? theme.palette.gradients.success
        : theme.palette.gradients.default,
    borderRadius: theme.shape.borderRadius,
    height: "100%",
  },
  backgroundImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundImage: `url(${tree})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: ({ expanded }: any) => (expanded ? "100%" : "26%"),
    backgroundPosition: "right bottom",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
}));

const CardAside = ({ children, className, expanded, success }) => {
  const classes = useStyles({ expanded, success });
  return (
    <div className={clsx(classes.thumb, className)}>
      <div className={classes.backgroundImage}>{children}</div>
    </div>
  );
};

CardAside.propTypes = {
  children: any,
  className: string,
  expanded: any,
  success: bool,
};

export default CardAside;

import React, { useState, useEffect, useMemo } from "react";
import clsx from "clsx";

import { useSearch } from "../../data";
import {
  makeStyles,
  Label,
  RadioButton,
  Checkbox,
  IconButton,
  FormControlLabel,
  FormControl,
  RadioGroup,
  Collapse,
  Icon,
  CircularProgress,
} from "../index";
import { withApollo } from "../../libs/apollo";
import { useQuery, gql } from "@apollo/client";
import { useEvent } from "../../data/useEvent";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  fieldset: {
    marginBottom: theme.spacing(3),
  },
  item: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: 2,
  },
  labelContainer: {
    display: "flex",
    alignItems: "center",
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
    marginBottom: theme.spacing(1),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  margined: {
    marginBottom: theme.spacing(1),
  },
  loading: {
    margin: "auto",
    marginTop: theme.spacing(2),
  },
}));

const transformToSearch = (values = {}) => {
  return Object.fromEntries(
    Object.entries(values).map(([edge, options]) => [
      edge,
      Object.entries(options)
        .filter(([, value]) => !!value)
        .map(([key]) => key),
    ])
  );
};

const FieldSet = ({ options, onQueryUpdate, edge }) => {
  const [expanded, setExpanded] = useState(true);

  const classes = useStyles();
  return (
    <FormControl component="fieldset" className={classes.fieldset}>
      <div className={classes.item}>
        <Label component="legend" className={classes.margined}>
          {edge}
        </Label>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={() => setExpanded(!expanded)}
          aria-expanded={expanded}
          aria-label="show more"
          size="small"
          color="secondary"
        >
          <Icon>chevron-down</Icon>
        </IconButton>
      </div>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <RadioGroup aria-label={edge} name={edge}>
          {Object.entries(options)?.map(([option, value]) => (
            <div className={classes.item} key={option}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={value}
                    value={value}
                    onClick={(e) => onQueryUpdate(option)(e.target.checked)}
                  />
                }
                label={option}
              />
              {/* <Amount label="34" /> */}
            </div>
          ))}
        </RadioGroup>
      </Collapse>
    </FormControl>
  );
};

const initialState = (data) => {
  if (!data) return {};
  return Object.fromEntries(
    data
      .filter((selector) => !!selector.edge)
      .filter(
        (selector) => selector.options.filter((option) => !!option).length > 0
      )
      .map((selector) => [
        selector.edge,
        selector.options.reduce(
          (prev, curr) => ({ ...prev, [curr]: false }),
          {}
        ),
      ])
  );
};

const Filter = ({ defaultValue = "", selectors = [] }) => {
  const classes = useStyles();
  const search = useSearch((state) => state.search);
  const [state, setState] = useState(initialState(selectors));
  useEvent("RESET_FILTER", () => setState(initialState(selectors)), []);

  useEffect(() => {
    search({
      [defaultValue]: transformToSearch(state),
    });
  }, [defaultValue, state, search]);

  const onQueryUpdate = (field) => (option) => (checked) => {
    setState((oldQuery) => ({
      ...oldQuery,
      [field]: {
        ...oldQuery[field],
        [option]: checked,
      },
    }));
  };

  return (
    <section className={classes.container}>
      {Object.entries(state).map(([edge, options]) => {
        return (
          <FieldSet
            key={edge}
            edge={edge}
            options={options}
            onQueryUpdate={onQueryUpdate(edge)}
          ></FieldSet>
        );
      })}
    </section>
  );
};

const FetchFilter = ({ defaultValue }) => {
  const classes = useStyles();

  const GET_SELECTORS = gql`
    {
      selectors(filter:"${defaultValue}") {
        edge
        field
        options
      }
    }
  `;
  const { loading, error, data = { selectors: [] } } = useQuery(GET_SELECTORS);

  return (
    <>
      {loading && <CircularProgress className={classes.loading} />}
      {!loading && !error && (
        <Filter defaultValue={defaultValue} selectors={data.selectors}></Filter>
      )}
    </>
  );
};
export default withApollo({ ssr: true })(FetchFilter);

import React, { useState } from "react";
import clsx from "clsx";
import {
  Card,
  Grid,
  Icon,
  makeStyles,
  Typography,
  CardParagraph,
  Step,
  StepLabel,
  Stepper,
  PrimaryButton,
  SecondaryButton,
  Select,
  Checkbox,
  PasswordField,
  TextField,
  Option,
  Collapse,
  FormControl,
  FormControlLabel,
  RadioGroup,
  ExternalLink as Link,
} from "./index";
const logo = "/images/gaia-x-logo-blue.svg";
const tree = "/images/gaia-x-tree.svg";
const treeSuccess = "/images/gaia-x-tree-success.svg";
const visa = "/images/visa.svg";
const mastercard = "/images/mastercard.svg";
const paypal = "/images/paypal.svg";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    boxShadow: theme.dimensions.shadowDialog,
    width: 960,
    margin: theme.spacing(0, 3),
  },
  cardContent: {
    display: "flex",
    flexDirection: "column",
    paddingTop: theme.spacing(2),
    [theme.breakpoints.up("md")]: {
      paddingLeft: theme.spacing(4),
      paddingTop: 0,
    },
  },
  thumb: {
    background: theme.palette.gradients.default,
    borderRadius: theme.shape.borderRadius,
    height: "calc(100% + 68px)",
    position: "relative",
  },
  backgroundImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundImage: `url(${tree})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "70%",
    backgroundPosition: "right bottom",
    "& h1": {
      marginBottom: theme.spacing(1),
    },
    "& p": {
      color: theme.palette.primary[900],
      paddingRight: 50,
    },
    "& i": {
      color: theme.palette.success.main,
      fontSize: theme.spacing(7),
      marginBottom: theme.spacing(3),
    },
  },
  thumbSuccess: {
    background: theme.palette.gradients.success,
  },
  backgroundImageSuccess: {
    backgroundImage: `url(${treeSuccess})`,
  },
  gridContainer: {
    marginBottom: theme.spacing(2),
    flexDirection: "column",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
      height: 386,
    },
  },
  logo: {
    height: theme.spacing(7),
    width: theme.spacing(7),
    marginBottom: theme.spacing(2),
  },
  stepper: {
    display: "flex",
    position: "absolute",
    bottom: `-${theme.spacing(2)}px`,
    left: 0,
    [theme.breakpoints.up("md")]: {
      display: "block",
      position: "absolute",
      right: "-14px",
      left: "auto",
      top: "auto",
      bottom: "auto",
    },
  },
  formGroup: {
    flexDirection: "row",
  },
  margined: {
    marginTop: theme.spacing(1),
  },
}));

function getSteps() {
  return [1, 2, 3, 4, 5, 6];
}

function getStepContent(step) {
  const classes = useStyles();
  const [state, setState] = useState({
    checked: false,
  });
  const [value, setValue] = useState("provider");
  const [expanded, setExpanded] = useState(false);

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleChangeRadio = (event) => {
    setValue(event.target.value);
  };

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  switch (step) {
    case 0:
      return (
        <>
          <CardParagraph>Please select your IdP Service.</CardParagraph>
          <FormControl component="form">
            <Select />
          </FormControl>
        </>
      );
    case 1:
      return (
        <>
          <CardParagraph>
            Please enter your IdP Service Credentials.
          </CardParagraph>
          <FormControl component="form">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField label="Email Address" />
              </Grid>
              <Grid item xs={12}>
                <PasswordField label="Password" />
              </Grid>
            </Grid>
            <FormControlLabel
              className={classes.margined}
              control={
                <Checkbox
                  checked={state.checked}
                  onChange={handleChange}
                  name="terms"
                  color="primary"
                />
              }
              label="I have read and accept the terms and conditions and the privacy policy of GAIA-X."
            />
          </FormControl>
        </>
      );
    case 2:
      return (
        <>
          <CardParagraph>Which role do you want to add?</CardParagraph>
          <FormControl component="form">
            <RadioGroup
              defaultValue="provider"
              value={value}
              aria-label="role"
              name="role"
              className={classes.formGroup}
            >
              <Option
                value="provider"
                label="Provider"
                icon={<Icon>provider</Icon>}
                onClick={handleChangeRadio}
              />
              <Option
                value="consumer"
                label="Consumer"
                icon={<Icon>user</Icon>}
              />
            </RadioGroup>
          </FormControl>
        </>
      );
    case 3:
      return (
        <>
          <CardParagraph>
            Please upload or enter your Provider Self-Description.
          </CardParagraph>
          <FormControl component="form">
            <Grid container spacing={3}>
              <Grid item xs={12} sm={12}>
                <PrimaryButton>
                  <Icon>upload</Icon>
                  Upload
                </PrimaryButton>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField label="Name" placeholder="Name" name="Name" />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Street and Number"
                  placeholder="Street and Number"
                  name="streetNumber"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="ZIP and city"
                  placeholder="ZIP and city"
                  name="zipCity"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Select label="Country" placeholder="Country" name="country" />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Email Address"
                  placeholder="Email Address"
                  name="emailAddress"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Phone Number"
                  placeholder="Phone Number"
                  name="phoneNumber"
                />
              </Grid>
            </Grid>
          </FormControl>
        </>
      );
    case 4:
      return (
        <>
          <CardParagraph>
            Please choose your preferred payment method.
          </CardParagraph>
          <FormControl component="form">
            <RadioGroup
              defaultValue="visa"
              value={value}
              aria-label="payment method"
              name="paymentMethod"
              className={classes.formGroup}
            >
              <Option
                onClick={handleExpandClick}
                aria-expanded={expanded}
                value="visa"
                icon={
                  <img
                    src={visa}
                    alt="Visa Card"
                    className={classes.paymentOption}
                  />
                }
              />
              <Option
                value="mastercard"
                icon={
                  <img
                    src={mastercard}
                    alt="Mastercard"
                    className={classes.paymentOption}
                  />
                }
              />
              <Option
                value="paypal"
                icon={
                  <img
                    src={paypal}
                    alt="PayPal"
                    className={classes.paymentOption}
                  />
                }
              />
              <Option
                label="invoice"
                value="invoice"
                icon={<Icon>empty-file</Icon>}
              />
            </RadioGroup>
          </FormControl>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <FormControl component="form">
              <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                  <TextField label="Owner" placeholder="Owner" name="owner" />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    label="Valid Until"
                    placeholder="Valid Until"
                    name="validUntil"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Select
                    label="Country"
                    placeholder="Country"
                    name="country"
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField label="CVC" placeholder="CVC" name="cvc" />
                </Grid>
                <Grid item xs={12} sm={12}>
                  <TextField
                    label="Card Number"
                    placeholder="Card Number"
                    name="cardNumber"
                  />
                </Grid>
              </Grid>
            </FormControl>
          </Collapse>
        </>
      );

    default:
      return (
        <CardParagraph>
          Please confirm your account by clicking on the link we have sent you
          via email.
        </CardParagraph>
      );
  }
}

const RegisterForm = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Card
      actions={
        <Grid container spacing={1} justify="flex-end">
          {activeStep === steps.length ? (
            <Grid item>
              <SecondaryButton onClick={handleReset} className={classes.button}>
                Reset
              </SecondaryButton>
            </Grid>
          ) : (
            <>
              <Grid item>
                <SecondaryButton
                  disabled={activeStep === 0}
                  onClick={handleBack}
                >
                  Back
                </SecondaryButton>
              </Grid>
              <Grid item>
                {activeStep === steps.length - 1 ? (
                  <Link href="/login">
                    <PrimaryButton>Login</PrimaryButton>
                  </Link>
                ) : (
                  <PrimaryButton onClick={handleNext}>Next</PrimaryButton>
                )}
              </Grid>
            </>
          )}
        </Grid>
      }
      className={classes.card}
    >
      <Grid container spacing={3} className={classes.gridContainer}>
        <Grid item sm={12} md={4}>
          <div
            className={clsx(classes.thumb, {
              [classes.thumbSuccess]: activeStep === steps[4],
            })}
          >
            {activeStep === steps[4] ? (
              <div
                className={clsx(
                  classes.backgroundImage,
                  classes.backgroundImageSuccess
                )}
              >
                <Icon>checkmark</Icon>
                <Typography variant="h1">Congratulations!</Typography>
                <Typography variant="body1">
                  Your registration is almost complete.
                </Typography>
              </div>
            ) : (
              <div className={classes.backgroundImage}>
                <img src={logo} alt="GAIA-X Logo" className={classes.logo} />
                <Typography variant="h1">Welcome to GAIA-X</Typography>
                <Typography variant="body1">
                  Create your account in a few steps and benefit of our secure
                  and transparent Federated Catalogue.
                </Typography>
              </div>
            )}
            <Stepper activeStep={activeStep} orientation="vertical">
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel />
                </Step>
              ))}
            </Stepper>
          </div>
        </Grid>
        <Grid item sm={12} md={8}>
          <div className={classes.cardContent}>
            {getStepContent(activeStep)}
          </div>
        </Grid>
      </Grid>
    </Card>
  );
};

export default RegisterForm;

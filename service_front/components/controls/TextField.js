import React, { createRef } from "react";
import { string, func, bool } from "prop-types";
import { TextField as MuiTextField, InputAdornment } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ErrorIcon from "@material-ui/icons/Error";
import Icon from "./Icon";

const useStyles = makeStyles((theme) => ({
  formControlRoot: {
    marginTop: 0,
    marginBottom: 0,
  },
  root: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
    "&:hover $notchedOutline": {
      borderColor: ({ error, success, disabled }) => {
        if (error === true) return theme.palette.error.main;
        else if (success === true) return theme.palette.success.main;
        else if (disabled === true) return theme.palette.action.disabled;
        return theme.palette.primary.main;
      },
    },
  },
  input: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
  },
  notchedOutline: {
    borderColor: ({ error, success }) => {
      if (error === true) return theme.palette.error.main;
      else if (success === true) return theme.palette.success.main;
      return theme.palette.primary[100];
    },
    transition: theme.transitions.create("border-color"),
    borderRadius: theme.shape.borderRadius,
  },
  focused: {
    "& $notchedOutline": {
      borderColor: ({ error, success }) => {
        if (error === true) return theme.palette.error.main;
        else if (success === true) return theme.palette.success.main;
        return theme.palette.primary.main;
      },
    },
  },
  helperTextRoot: {},
  helperTextContained: {
    position: "absolute",
    bottom: "-18px",
    color: ({ error, disabled }) => {
      if (error === true) return theme.palette.error.main;
      else if (disabled === true) return theme.palette.action.disabled;
      return theme.palette.primary.main;
    },
  },
  positionEnd: {
    "& i": {
      width: theme.spacing(2),
      height: theme.spacing(2),
      color: ({ error, success }) => {
        if (error === true) return theme.palette.error.main;
        else if (success === true) return theme.palette.success.main;
        return theme.palette.primary.main;
      },
    },
  },
}));

const TextField = ({
  onChange,
  name,
  label,
  placeholder,
  className,
  error,
  success,
  disabled,
  fullWidth,
  helperText,
  ...rest
}) => {
  const ref = createRef();
  const classes = useStyles({ error, success, disabled });

  const adornment = () => {
    if (error === true)
      return (
        <InputAdornment
          position="end"
          classes={{ positionEnd: classes.positionEnd }}
        >
          <Icon>error-circle</Icon>
        </InputAdornment>
      );
    else if (success === true)
      return (
        <InputAdornment
          position="end"
          classes={{ positionEnd: classes.positionEnd }}
        >
          <Icon>checkmark</Icon>
        </InputAdornment>
      );
    return null;
  };

  return (
    <MuiTextField
      {...rest}
      fullWidth={fullWidth}
      type="text"
      onChange={onChange}
      name={name}
      label={label}
      placeholder={placeholder}
      disabled={disabled}
      color="primary"
      variant="outlined"
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck="false"
      ref={ref}
      helperText={helperText}
      className={className}
      classes={{ root: classes.formControlRoot }}
      InputProps={{
        classes: {
          root: classes.root,
          notchedOutline: classes.notchedOutline,
          input: classes.input,
          focused: classes.focused,
        },
        endAdornment: adornment(),
      }}
      FormHelperTextProps={{
        classes: {
          root: classes.helperTextRoot,
          contained: classes.helperTextContained,
        },
      }}
    ></MuiTextField>
  );
};

TextField.propTypes = {
  placeholder: string,
  label: string,
  name: string,
  onChange: func,
  className: string,
  error: bool,
  success: bool,
  fullWidth: bool,
  disabled: bool,
  helperText: string,
};

TextField.defaultProps = {
  fullWidth: true,
  error: false,
  success: false,
  disabled: false,
};

export default TextField;

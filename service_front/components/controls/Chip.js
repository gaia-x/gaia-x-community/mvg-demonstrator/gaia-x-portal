import React from "react";
import { func, string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Chip as MuiChip } from "@material-ui/core";
import Icon from "./Icon";

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: theme.spacing(28),
    // textOverflow: "ellipsis",
    // overflow: "hidden",
    // [theme.breakpoints.down("md")]: {
    //   maxWidth: theme.spacing(11),
    //   textOverflow: "ellipsis",
    //   overflow: "hidden",
    // },
  },
  label: {
    fontSize: theme.typography.size.s,
    fontWeight: theme.typography.fontWeightMedium,
  },
  colorSecondary: {
    backgroundColor: theme.palette.secondary[100],
    color: theme.palette.primary.main,
    "&:hover": {
      backgroundColor: ({ hasIcon }) =>
        hasIcon ? theme.palette.secondary.light : theme.palette.secondary[100],
      cursor: ({ hasIcon }) => (hasIcon ? "pointer" : "default"),
    },
  },
  iconColorSecondary: {
    color: theme.palette.primary.main,
  },
  iconSmall: {
    fontSize: theme.typography.size.xs,
    height: theme.typography.size.xs,
    width: theme.typography.size.xs,
    marginLeft: theme.spacing(1),
    marginRight: `-${theme.spacing(0.25)}px`,
  },
  deleteIconColorSecondary: {
    color: theme.palette.secondary.main,
    "&:hover": {
      color: theme.palette.secondary.dark,
    },
  },
}));

const Chip = ({ hasIcon, label, onClick, onDelete, size, ...rest }) => {
  const classes = useStyles({ hasIcon });

  return (
    <MuiChip
      {...rest}
      icon={hasIcon ? <Icon>link</Icon> : null}
      label={label}
      size={size}
      onClick={onClick}
      onDelete={onDelete}
      color="secondary"
      classes={{
        root: classes.root,
        label: classes.label,
        colorSecondary: classes.colorSecondary,
        iconColorSecondary: classes.iconColorSecondary,
        iconSmall: classes.iconSmall,
        deleteIconColorSecondary: classes.deleteIconColorSecondary,
      }}
    />
  );
};

Chip.propTypes = {
  hasIcon: bool,
  label: string.isRequired,
  onClick: func,
  onDelete: func,
  size: string,
};

Chip.defaultProps = {
  hasIcon: false,
};

export default Chip;

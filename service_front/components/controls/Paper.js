import React from "react";
import { any, arrayOf, element, oneOfType, string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Paper as MuiPaper } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: theme.shape.borderRadius,
  },
  elevation1: {
    boxShadow: theme.dimensions.shadowCard,
  },
  outlined: {
    border: `1px solid ${theme.palette.primary[100]} !important`,
  },
}));

const Paper = ({ className, children, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiPaper
      {...rest}
      elevation={1}
      classes={{
        root: classes.root,
        elevation1: classes.elevation1,
        outlined: classes.outlined,
      }}
      className={className}
    >
      {children}
    </MuiPaper>
  );
};

Paper.propTypes = {
  children: oneOfType([arrayOf(any), element, string]),
  className: string,
};

export default Paper;

import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import Paper from "./Paper";

const useStyles = makeStyles((theme) => ({
  tableContainer: {
    borderRadius: theme.shape.borderRadius,
  },
  table: {
    "& th, & td": {
      fontSize: theme.typography.size.m,
      borderBottom: 0,
    },
  },
  white: {
    color: theme.palette.common.white,
  },
}));

export const PrimaryColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table
          aria-label="Primary Color Palette Table"
          className={classes.table}
        >
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.primary[50] }}>
              <TableCell>theme.palette.primary[50]</TableCell>
              <TableCell align="right">{theme.palette.primary[50]}</TableCell>
              <TableCell align="right">224, 231, 233</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[100] }}>
              <TableCell>theme.palette.primary[100]</TableCell>
              <TableCell align="right">{theme.palette.primary[100]}</TableCell>
              <TableCell align="right">179, 196, 200</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[200] }}>
              <TableCell>theme.palette.primary[200]</TableCell>
              <TableCell align="right">{theme.palette.primary[200]}</TableCell>
              <TableCell align="right">128, 157, 163</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary.light }}>
              <TableCell className={classes.white}>
                theme.palette.primary.light
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary.light}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                77, 118, 126
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[400] }}>
              <TableCell className={classes.white}>
                theme.palette.primary[400]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary[400]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                38, 88, 98
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary.main }}>
              <TableCell className={classes.white}>
                theme.palette.primary.main
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary.main}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 59, 70
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[600] }}>
              <TableCell className={classes.white}>
                theme.palette.primary[600]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary[600]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 53, 63
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary.dark }}>
              <TableCell className={classes.white}>
                theme.palette.primary.dark
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary.dark}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 45, 55
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[800] }}>
              <TableCell className={classes.white}>
                theme.palette.primary[800]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary[800]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 38, 47
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.primary[900] }}>
              <TableCell className={classes.white}>
                theme.palette.primary[900]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.primary[900]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 25, 32
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export const SecondaryColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table
          aria-label="Secondary Color Palette Table"
          className={classes.table}
        >
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.secondary[50] }}>
              <TableCell>theme.palette.secondary[50]</TableCell>
              <TableCell align="right">{theme.palette.secondary[50]}</TableCell>
              <TableCell align="right">224, 246, 249</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[100] }}>
              <TableCell>theme.palette.secondary[100]</TableCell>
              <TableCell align="right">
                {theme.palette.secondary[100]}
              </TableCell>
              <TableCell align="right">179, 232, 241</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[200] }}>
              <TableCell>theme.palette.secondary[200]</TableCell>
              <TableCell align="right">
                {theme.palette.secondary[200]}
              </TableCell>
              <TableCell align="right">128, 217, 232</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow
              style={{ backgroundColor: theme.palette.secondary.light }}
            >
              <TableCell>theme.palette.secondary.light</TableCell>
              <TableCell align="right">
                {theme.palette.secondary.light}
              </TableCell>
              <TableCell align="right">77, 201, 223</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[400] }}>
              <TableCell>theme.palette.secondary[400]</TableCell>
              <TableCell align="right">
                {theme.palette.secondary[400]}
              </TableCell>
              <TableCell align="right">38, 190, 216</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary.main }}>
              <TableCell>theme.palette.secondary.main</TableCell>
              <TableCell align="right">
                {theme.palette.secondary.main}
              </TableCell>
              <TableCell align="right">0, 178, 209</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[600] }}>
              <TableCell className={classes.white}>
                theme.palette.secondary[600]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.secondary[600]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 171, 204
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary.dark }}>
              <TableCell className={classes.white}>
                theme.palette.secondary.dark
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.secondary.dark}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 162, 198
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[800] }}>
              <TableCell className={classes.white}>
                theme.palette.secondary[800]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.secondary[800]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 153, 192
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.secondary[900] }}>
              <TableCell className={classes.white}>
                theme.palette.secondary[900]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.secondary[900]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 138, 181
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export const NeutralColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table
          aria-label="Neutral Color Palette Table"
          className={classes.table}
        >
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.neutral[50] }}>
              <TableCell>theme.palette.neutral[50]</TableCell>
              <TableCell align="right">{theme.palette.neutral[50]}</TableCell>
              <TableCell align="right">248, 251, 252</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[100] }}>
              <TableCell>theme.palette.neutral[100]</TableCell>
              <TableCell align="right">{theme.palette.neutral[100]}</TableCell>
              <TableCell align="right">237, 245, 248</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[200] }}>
              <TableCell>theme.palette.neutral[200]</TableCell>
              <TableCell align="right">{theme.palette.neutral[200]}</TableCell>
              <TableCell align="right">226, 239, 243</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral.light }}>
              <TableCell>theme.palette.neutral.light</TableCell>
              <TableCell align="right">{theme.palette.neutral.light}</TableCell>
              <TableCell align="right">214, 233, 238</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[400] }}>
              <TableCell>theme.palette.neutral[400]</TableCell>
              <TableCell align="right">{theme.palette.neutral[400]}</TableCell>
              <TableCell align="right">205, 228, 234</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral.main }}>
              <TableCell>theme.palette.neutral.main</TableCell>
              <TableCell align="right">{theme.palette.neutral.main}</TableCell>
              <TableCell align="right">196, 223, 230</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[600] }}>
              <TableCell>theme.palette.neutral[600]</TableCell>
              <TableCell align="right">{theme.palette.neutral[600]}</TableCell>
              <TableCell align="right">190, 219, 227</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral.dark }}>
              <TableCell>theme.palette.neutral.dark</TableCell>
              <TableCell align="right">{theme.palette.neutral.dark}</TableCell>
              <TableCell align="right">182, 215, 223</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[800] }}>
              <TableCell>theme.palette.neutral[800]</TableCell>
              <TableCell align="right">{theme.palette.neutral[800]}</TableCell>
              <TableCell align="right">175, 210, 219</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.neutral[900] }}>
              <TableCell>theme.palette.neutral[900]</TableCell>
              <TableCell align="right">{theme.palette.neutral[900]}</TableCell>
              <TableCell align="right">162, 202, 213</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export const GreyColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="Grey Color Palette Table" className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.grey[50] }}>
              <TableCell>theme.palette.grey[50]</TableCell>
              <TableCell align="right">{theme.palette.grey[50]}</TableCell>
              <TableCell align="right">250, 250, 250</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[100] }}>
              <TableCell>theme.palette.grey[100]</TableCell>
              <TableCell align="right">{theme.palette.grey[100]}</TableCell>
              <TableCell align="right">245, 245, 245</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[200] }}>
              <TableCell>theme.palette.grey[200]</TableCell>
              <TableCell align="right">{theme.palette.grey[200]}</TableCell>
              <TableCell align="right">238, 238, 238</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[300] }}>
              <TableCell>theme.palette.grey[300]</TableCell>
              <TableCell align="right">{theme.palette.grey[300]}</TableCell>
              <TableCell align="right">224, 224, 224</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[400] }}>
              <TableCell>theme.palette.grey[400]</TableCell>
              <TableCell align="right">{theme.palette.grey[400]}</TableCell>
              <TableCell align="right">189, 189, 189</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[500] }}>
              <TableCell>theme.palette.grey[500]</TableCell>
              <TableCell align="right">{theme.palette.grey[500]}</TableCell>
              <TableCell align="right">158, 158, 158</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[600] }}>
              <TableCell className={classes.white}>
                theme.palette.grey[600]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey[600]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                117, 117, 117
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[700] }}>
              <TableCell className={classes.white}>
                theme.palette.grey[700]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey[700]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                97, 97, 97
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[800] }}>
              <TableCell className={classes.white}>
                theme.palette.grey[800]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey[800]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                66, 66, 66
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[900] }}>
              <TableCell className={classes.white}>
                theme.palette.grey[900]
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey[900]}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                33, 33, 33
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey.A100 }}>
              <TableCell>theme.palette.grey.A100</TableCell>
              <TableCell align="right">{theme.palette.grey.A100}</TableCell>
              <TableCell align="right">213, 213, 213</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey.A200 }}>
              <TableCell>theme.palette.grey.A200</TableCell>
              <TableCell align="right">{theme.palette.grey.A200}</TableCell>
              <TableCell align="right">170, 170, 170</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey.A400 }}>
              <TableCell className={classes.white}>
                theme.palette.grey.A400
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey.A400}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                48, 48, 48
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey.A700 }}>
              <TableCell className={classes.white}>
                theme.palette.grey.A700
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.grey.A700}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                97, 97, 97
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export const TextColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table aria-label="Text Color Palette Table" className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.grey[900] }}>
              <TableCell className={classes.white}>
                theme.palette.text.primary
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.text.primary}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                33, 33, 33
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[700] }}>
              <TableCell className={classes.white}>
                theme.palette.text.secondary
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.text.secondary}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                97, 97, 97
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.grey[400] }}>
              <TableCell>theme.palette.text.disabled</TableCell>
              <TableCell align="right">{theme.palette.text.disabled}</TableCell>
              <TableCell align="right">189, 189, 189</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export const UtilityColorPalette = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Paper>
      <TableContainer className={classes.tableContainer}>
        <Table
          aria-label="Utility Color Palette Table"
          className={classes.table}
        >
          <TableHead>
            <TableRow>
              <TableCell>Key</TableCell>
              <TableCell align="right">HEX</TableCell>
              <TableCell align="right">RGB</TableCell>
              <TableCell align="right">Font Color</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow style={{ backgroundColor: theme.palette.error.light }}>
              <TableCell>theme.palette.error.light</TableCell>
              <TableCell align="right">{theme.palette.error.light}</TableCell>
              <TableCell align="right">255, 134, 124</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.error.main }}>
              <TableCell>theme.palette.error.main</TableCell>
              <TableCell align="right">{theme.palette.error.main}</TableCell>
              <TableCell align="right">239, 83, 80</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.error.dark }}>
              <TableCell className={classes.white}>
                theme.palette.error.dark
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.error.dark}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                182, 24, 39
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.warning.light }}>
              <TableCell>theme.palette.warning.light</TableCell>
              <TableCell align="right">{theme.palette.warning.light}</TableCell>
              <TableCell align="right">255, 217, 91</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.warning.main }}>
              <TableCell>theme.palette.warning.main</TableCell>
              <TableCell align="right">{theme.palette.warning.main}</TableCell>
              <TableCell align="right">255, 167, 38</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.warning.dark }}>
              <TableCell>theme.palette.warning.dark</TableCell>
              <TableCell align="right">{theme.palette.warning.dark}</TableCell>
              <TableCell align="right">219, 132, 0</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.success.light }}>
              <TableCell>theme.palette.success.light</TableCell>
              <TableCell align="right">{theme.palette.success.light}</TableCell>
              <TableCell align="right">152, 238, 153</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.success.main }}>
              <TableCell>theme.palette.success.main</TableCell>
              <TableCell align="right">{theme.palette.success.main}</TableCell>
              <TableCell align="right">102, 187, 106</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.success.dark }}>
              <TableCell className={classes.white}>
                theme.palette.success.dark
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.success.dark}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                51, 138, 62
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.info.light }}>
              <TableCell>theme.palette.info.light</TableCell>
              <TableCell align="right">{theme.palette.info.light}</TableCell>
              <TableCell align="right">128, 214, 255</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.info.main }}>
              <TableCell>theme.palette.info.main</TableCell>
              <TableCell align="right">{theme.palette.info.main}</TableCell>
              <TableCell align="right">66, 165, 245</TableCell>
              <TableCell align="right">{theme.palette.text.primary}</TableCell>
            </TableRow>
            <TableRow style={{ backgroundColor: theme.palette.info.dark }}>
              <TableCell className={classes.white}>
                theme.palette.info.dark
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.info.dark}
              </TableCell>
              <TableCell align="right" className={classes.white}>
                0, 119, 194
              </TableCell>
              <TableCell align="right" className={classes.white}>
                {theme.palette.common.white}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

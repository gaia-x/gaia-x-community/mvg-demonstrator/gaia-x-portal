import React, { forwardRef } from "react";
import { string } from "prop-types";
import clsx from "clsx";

const Icon = forwardRef((props, ref) => {
  return (
    <i {...props} className={clsx("gaia-x-icons", props.className)} ref={ref}>
      {props.children}
    </i>
  );
});

Icon.propTypes = {
  className: string,
  children: string
};

export default Icon;

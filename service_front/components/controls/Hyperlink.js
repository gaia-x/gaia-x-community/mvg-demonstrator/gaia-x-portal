import React from "react";
import clsx from "clsx";
import { any, string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  link: {
    fontSize: theme.typography.size.m,
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.primary.main,
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline",
    },
  },
}));

const Hyperlink = ({ className, children }) => {
  const classes = useStyles();

  return <span className={clsx(classes.link, className)}>{children}</span>;
};

Hyperlink.propTypes = {
  className: string,
  children: any,
};

export default Hyperlink;

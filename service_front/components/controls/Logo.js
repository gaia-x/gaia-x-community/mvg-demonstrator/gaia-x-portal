import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "./Card";
import Grid from "./Grid";
const logoBlue = "images/gaia-x-logo-blue.png";
const logoWhite = "images/gaia-x-logo-white.png";
const tree = "images/gaia-x-tree.png";

const useStyles = makeStyles((theme) => ({
  section: {
    padding: theme.spacing(5),
    borderRadius: theme.shape.borderRadius,
  },
  card: {
    marginBottom: 0,
    height: "fit-content",
    width: "fit-content",
    "& img": {
      borderRadius: theme.shape.borderRadius,
    },
  },
}));

const Logo = () => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <section
          className={classes.section}
          style={{ backgroundColor: theme.palette.primary[50] }}
        >
          <Card hasHover className={classes.card}>
            <img src={logoBlue} alt="GAIA-X Logo" />
          </Card>
          <h2>GAIA-X Logo in blue</h2>
          <p>
            The GAIA-X logo in blue is used on light backgrounds, for instance
            in the login and register panel.
          </p>
        </section>
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <section
          className={classes.section}
          style={{ backgroundColor: theme.palette.neutral[400] }}
        >
          <Card
            hasHover
            className={classes.card}
            style={{ backgroundColor: theme.palette.primary.main }}
          >
            <img src={logoWhite} alt="GAIA-X Logo" />
          </Card>
          <h2>GAIA-X Logo in white</h2>
          <p>
            The white variant is used on dark backgrounds, for instance in the
            vertical navigation bar.
          </p>
        </section>
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <section
          className={classes.section}
          style={{ backgroundColor: theme.palette.secondary[200] }}
        >
          <Card hasHover className={classes.card}>
            <img src={tree} alt="GAIA-X Tree" />
          </Card>
          <h2>GAIA-X Tree</h2>
          <p>
            The GAIA-X Tree is also used on the main content background to
            reflect the brand.
          </p>
        </section>
      </Grid>
    </Grid>
  );
};

export default Logo;

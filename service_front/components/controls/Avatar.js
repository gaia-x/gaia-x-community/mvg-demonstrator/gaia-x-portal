import React from "react";
import { node, string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { Avatar as MuiAvatar } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.common.white,
    fontSize: theme.typography.size.l,
    fontWeight: theme.typography.fontWeightMedium,
    boxShadow: ({ boxShadow }) =>
      boxShadow ? theme.dimensions.shadowLarge : "none",
    height: ({ size }) => {
      if (size === "large") return theme.spacing(6);
    },
    width: ({ size }) => {
      if (size === "large") return theme.spacing(6);
    },
  },
  img: {
    width: theme.spacing(6),
    height: "auto",
  },
}));

const Avatar = ({ alt, src, boxShadow, className, size, ...rest }) => {
  const classes = useStyles({ boxShadow, size });

  return (
    <MuiAvatar
      alt={alt}
      src={src}
      classes={{ root: classes.root, img: classes.img }}
      className={className}
      size={size}
      {...rest}
    />
  );
};

Avatar.propTypetrs = {
  alt: string.isRequired,
  boxShadow: bool,
  src: node,
  className: string,
  size: string,
};

export default Avatar;

import React, { useState } from "react";
import { string, any, bool } from "prop-types";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { Tooltip, ClickAwayListener, Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  tooltip: {
    backgroundColor: ({ error, success, warning }) => {
      if (error === true) return fade(theme.palette.error.light, 0.2);
      else if (success === true) return fade(theme.palette.success.light, 0.2);
      else if (warning === true) return fade(theme.palette.warning.light, 0.2);
      return theme.palette.background.paper;
    },
    borderColor: ({ error, success, warning }) => {
      if (error === true) return theme.palette.error.light;
      else if (success === true) return theme.palette.success.light;
      else if (warning === true) return theme.palette.warning.light;
      return "tranparent";
    },
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(2),
    boxShadow: theme.dimensions.shadowCard,
    color: theme.palette.text.primary,
    fontSize: theme.typography.size.xs,
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const InlineDialog = (props) => {
  const { title, children, error, success, warning, ...rest } = props;
  const classes = useStyles({ error, success, warning });
  const [open, setOpen] = useState(false);

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  return (
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <div>
        <Tooltip
          {...rest}
          title={title}
          placement="right"
          classes={{ tooltip: classes.tooltip }}
          error={error ? 1 : 0}
          success={success ? 1 : 0}
          warning={warning ? 1 : 0}
          PopperProps={{
            disablePortal: true,
          }}
          onClose={handleTooltipClose}
          open={open}
          disableFocusListener
          disableHoverListener
          disableTouchListener
        >
          <Box onClick={handleTooltipOpen}>{children}</Box>
        </Tooltip>
      </div>
    </ClickAwayListener>
  );
};

InlineDialog.propTypes = {
  title: string.isRequired,
  children: any.isRequired,
  error: bool,
  success: bool,
  warning: bool,
};

export default InlineDialog;

import React from "react";
import { any } from "prop-types";
import { FormControl as MuiFormControl } from "@material-ui/core";

const FormControl = ({ className, children }) => {
  return <MuiFormControl className={className}>{children}</MuiFormControl>;
};

FormControl.propTypes = {
  children: any,
  className: any,
};

export default FormControl;

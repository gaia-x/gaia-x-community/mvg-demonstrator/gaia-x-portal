import React, { forwardRef } from "react";
import { func, bool } from "prop-types";
import { Checkbox as MuiCheckbox } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "./Icon";

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary[100],
    transition: theme.transitions.create("color"),
    padding: theme.spacing(1),
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
}));

const Checkbox = forwardRef((props, ref) => {
  const classes = useStyles();

  return (
    <MuiCheckbox
      {...props}
      ref={ref}
      checked={props.checked}
      onChange={props.onChange}
      color="primary"
      size="small"
      inputProps={{ "aria-label": "checkbox" }}
      classes={{ root: classes.root }}
      icon={<Icon>checkbox</Icon>}
      checkedIcon={<Icon>checkbox-checked</Icon>}
    />
  );
});

Checkbox.propTypes = {
  checked: bool,
  onChange: func,
};

export default Checkbox;

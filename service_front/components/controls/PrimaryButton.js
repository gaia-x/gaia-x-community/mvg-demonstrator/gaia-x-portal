import React, { forwardRef } from "react";
import { any, func, bool } from "prop-types";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: theme.dimensions.inputHeight,
    letterSpacing: theme.typography.letterSpacing,
    borderRadius: theme.shape.borderRadius,
    padding: "6px 12px",
    transition: theme.transitions.create("all"),
    "&:hover": {
      backgroundColor: ({ dark }) =>
        dark ? theme.palette.primary.dark : theme.palette.primary[200],
      color: theme.palette.common.white,
      "& i": {
        color: theme.palette.common.white,
      },
    },
    "&:disabled": {
      backgroundColor: theme.palette.text.disabled,
    },
    "& i": {
      transition: theme.transitions.create("all"),
      marginRight: theme.spacing(1),
      fontSize: theme.typography.size.m,
      color: ({ dark }) =>
      dark ? theme.palette.common.white : theme.palette.primary.main,
    },
  },
  containedPrimary: {
    backgroundColor: ({ dark }) =>
      dark ? theme.palette.primary.main : theme.palette.primary[50],
    color: ({ dark }) =>
      dark ? theme.palette.common.white : theme.palette.primary.main,
  },
}));

const PrimaryButton = forwardRef((props, ref) => {
  const { onClick, children, dark, ...rest } = props;
  const classes = useStyles({ dark });
  return (
    <Button
      classes={{
        root: classes.root,
        containedPrimary: classes.containedPrimary,
      }}
      variant="contained"
      color="primary"
      dark={dark ? 1 : 0}
      disableElevation
      onClick={onClick}
      ref={ref}
      {...rest}
    >
      {children}
    </Button>
  );
});

PrimaryButton.propTypes = {
  children: any.isRequired,
  onClick: func,
  dark: bool,
};

PrimaryButton.defaultPropTypes = {
  dark: false,
};

export default PrimaryButton;

import React from "react";
import { node, element } from "prop-types";
import { Tooltip as MuiTooltip } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  tooltip: {
    fontSize: theme.typography.size.xs,
    padding: theme.spacing(0.25, 1, 0.5, 1),
  },
  tooltipArrow: {
    backgroundColor: theme.palette.primary.dark,
    borderRadius: theme.shape.borderRadius,
  },
  arrow: {
    color: theme.palette.primary.main,
    marginBottom: "-0.6em !important",
  },
}));

const Tooltip = ({ title, children, placement, ...rest }) => {
  const classes = useStyles();
  return (
    <MuiTooltip
      {...rest}
      title={title}
      arrow
      placement="top"
      disableTouchListener
      classes={{
        tooltip: classes.tooltip,
        tooltipArrow: classes.tooltipArrow,
        arrow: classes.arrow,
      }}
    >
      {children}
    </MuiTooltip>
  );
};

Tooltip.propTypes = {
  title: node.isRequired,
  children: element.isRequired,
};
export default Tooltip;

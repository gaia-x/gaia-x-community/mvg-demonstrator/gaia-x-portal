import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "./Card";
import Grid from "./Grid";
const europe = "/images/gaia-x-europe.jpg";
const register = "/images/gaia-x-register.jpg";
const nodes = "/images/gaia-x-hero-nodes.jpg";

const useStyles = makeStyles((theme) => ({
  section: {
    padding: theme.spacing(5),
    margin: theme.spacing(5, 0),
    borderRadius: theme.shape.borderRadius,
  },
  card: {
    marginBottom: 0,
    "& img": {
      borderRadius: theme.shape.borderRadius,
      width: "100%",
      height: "auto",
    },
  },
}));

const Images = () => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <>
      <section
        className={classes.section}
        style={{ backgroundColor: theme.palette.primary[50] }}
      >
        <h2>Login Background</h2>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <img src={europe} alt="GAIA-X Login" />
            </Card>
          </Grid>
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{ backgroundColor: theme.palette.neutral[400] }}
      >
        <h2>Registration Background</h2>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <img src={register} alt="GAIA-X Register" />
            </Card>
          </Grid>
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{ backgroundColor: theme.palette.secondary[200] }}
      >
        <h2>Search Container Background</h2>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <img src={nodes} alt="GAIA-X Nodes" />
            </Card>
          </Grid>
        </Grid>
      </section>
    </>
  );
};

export default Images;

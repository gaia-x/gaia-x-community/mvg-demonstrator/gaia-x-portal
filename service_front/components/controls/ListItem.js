import React from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { ListItem as MuiListItem } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {},
  divider: {
    borderBottomColor: theme.palette.primary[50],
  },
}));

const ListItem = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <MuiListItem
      divider
      className={className}
      classes={{ root: classes.root, divider: classes.divider }}
      {...rest}
    />
  );
};

ListItem.propTypes = {
  className: string,
};

export default ListItem;

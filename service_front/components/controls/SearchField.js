import React, { createRef, useState } from "react";
import { any, string, func, bool } from "prop-types";
import { InputAdornment, TextField as MuiTextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "./Icon";
import IconButton from "./IconButton";

const useStyles = makeStyles((theme) => ({
  formControlRoot: {
    marginTop: 0,
    marginBottom: 0,
  },
  root: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: theme.shape.borderRadius,
    "&:hover $notchedOutline": {
      borderColor: theme.palette.primary.main,
    },
    "& i": {
      color: theme.palette.secondary.main,
    },
  },
  input: {
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.background.paper,
    "&:-webkit-autofill": {
      borderRadius: 0,
    },
  },
  notchedOutline: {
    borderColor: theme.palette.primary[100],
    transition: theme.transitions.create("border-color"),
    borderRadius: theme.shape.borderRadius,
  },
}));

const SearchField = ({
  fullWidth,
  disabled,
  onChange,
  label,
  name,
  className,
  value,
  onClose,
  ...rest
}) => {
  const ref = createRef();
  const classes = useStyles();

  return (
    <MuiTextField
      {...rest}
      fullWidth={fullWidth}
      value={value}
      label={label}
      placeholder={name}
      className={className}
      ref={ref}
      onChange={onChange}
      disabled={disabled}
      type="search"
      color="primary"
      margin="none"
      variant="outlined"
      autoCorrect="off"
      autoCapitalize="off"
      spellCheck="false"
      classes={{ root: classes.formControlRoot }}
      InputLabelProps={{
        shrink: true,
      }}
      InputProps={{
        classes: {
          root: classes.root,
          input: classes.input,
          notchedOutline: classes.notchedOutline,
          focused: classes.focused,
        },
        startAdornment: (
          <InputAdornment position="start">
            <Icon>search</Icon>
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end">
            <IconButton size="small" onClick={onClose}>
              <Icon style={{ fontSize: 14 }}>close</Icon>
            </IconButton>
          </InputAdornment>
        ),
      }}
    ></MuiTextField>
  );
};

SearchField.propTypes = {
  label: string,
  name: string,
  placeholder: string,
  onChange: func,
  onClose: func,
  className: string,
  fullWidth: bool,
  disabled: bool,
  value: any,
  ref: any,
};

SearchField.defaultProps = {
  fullWidth: true,
  disabled: false,
};

export default SearchField;

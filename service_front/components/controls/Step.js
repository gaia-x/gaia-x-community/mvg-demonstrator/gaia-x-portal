import React from "react";
import { bool, any, number, element, node, string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Step as MuiStep,
  StepLabel as MuiStepLabel,
  Stepper as MuiStepper,
  StepButton as MuiStepButton,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: "50%",
    margin: theme.spacing(0, 3),
    [theme.breakpoints.up("md")]: {
      margin: theme.spacing(3, 0),
    },
  },
  vertical: {
    [theme.breakpoints.up("md")]: {
      margin: 0,
    },
  },
}));

export const Step = (props) => {
  const classes = useStyles();
  return (
    <MuiStep
      classes={{
        root: classes.root,
        vertical: classes.vertical,
      }}
      {...props}
    >
      {props.children}
    </MuiStep>
  );
};

const useStylesLabel = makeStyles((theme) => ({
  iconContainer: {
    fontWeight: theme.typography.fontWeightMedium,
  },
  icon: {
    color: theme.palette.primary[100],
    height: theme.spacing(4),
    width: theme.spacing(4),
  },
}));

export const StepLabel = ({ active, completed, children, ...rest }) => {
  const classes = useStylesLabel({ active });
  return (
    <MuiStepLabel
      classes={{ iconContainer: classes.iconContainer }}
      StepIconProps={{
        classes: { root: classes.icon },
      }}
      active={active}
      completed={completed}
      {...rest}
    >
      {children}
    </MuiStepLabel>
  );
};

StepLabel.propTypes = {
  active: bool,
  completed: bool,
  children: any,
};

const useStylesStepper = makeStyles((theme) => ({
  root: {
    "& span": {
      borderColor: "transparent",
    },
  },
  vertical: {
    position: "absolute",
    zIndex: 1,
    backgroundColor: "transparent",
    top: 0,
    left: theme.spacing(30.75),
  },
  horizontal: {
    backgroundColor: "transparent",
    marginTop: `-${theme.spacing(8)}px`,
    padding: theme.spacing(0, 3),
  },
}));

export const Stepper = ({
  activeStep,
  alternativeLabel,
  children,
  connector,
  nonLinear,
  orientation,
  ...rest
}) => {
  const classes = useStylesStepper();
  return (
    <MuiStepper
      activeStep={activeStep}
      alternativeLabel={alternativeLabel}
      classes={{
        root: classes.root,
        vertical: classes.vertical,
        horizontal: classes.horizontal,
      }}
      {...rest}
      connector={connector}
      nonLinear={nonLinear}
      orientation={orientation}
    >
      {children}
    </MuiStepper>
  );
};

Stepper.propTypes = {
  activeStep: number,
  alternativeLabel: bool,
  children: node,
  connector: element,
  nonLinear: bool,
  orientation: string,
};

Stepper.defaultProps = {
  activeStep: 0,
  alternativeLabel: false,
  nonLinear: false,
  orientation: "horizontal",
};

const useStylesStepButton = makeStyles((theme) => ({
  root: {
    backgroundColor: "transparent",
    padding: theme.spacing(0, 3),
    "& span": {
      borderColor: "transparent",
    },
    "& svg": {
      color: theme.palette.primary[100],
      height: theme.spacing(4),
      width: theme.spacing(4),
    },
  },
}));

export const StepButton = ({ children, icon, optional, ...rest }) => {
  const classes = useStylesStepButton();
  return (
    <MuiStepButton
      classes={{
        root: classes.root,
      }}
      icon={icon}
      optional={optional}
      {...rest}
    >
      {children}
    </MuiStepButton>
  );
};

StepButton.propTypes = {
  children: node,
  icon: node,
  optional: node,
};

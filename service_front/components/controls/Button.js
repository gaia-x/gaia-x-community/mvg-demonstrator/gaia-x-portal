import React from "react";
import { object, string } from "prop-types";
import { Button as MuiButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "./Icon";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 2, 1.25, 2),
    width: theme.spacing(10),
    height: theme.spacing(8),
    borderRadius: 0,
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
    [theme.breakpoints.down("xs")]: {
      width: theme.spacing(7),
      borderRadius: theme.shape.borderRadius,
    },
  },
  label: {
    color: theme.palette.neutral.light,
    textTransform: "capitalize",
    fontSize: "0.7rem",
    letterSpacing: theme.typography.letterSpacing,
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  flex: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    fontSize: `${theme.spacing(3)}px !important`,
    color: theme.palette.neutral.light,
    marginBottom: theme.spacing(0.5),
  },
}));

const Button = React.forwardRef(({ label, icon }, ref) => {
  const classes = useStyles();

  return (
    <MuiButton
      ref={ref}
      variant="contained"
      color="primary"
      disableElevation
      classes={{ root: classes.root }}
    >
      <div className={classes.flex}>
        <Icon className={classes.icon}>{icon}</Icon>
        {label && <span className={classes.label}>{label}</span>}
      </div>
    </MuiButton>
  );
});

Button.propTypes = {
  label: string.isRequired,
  icon: string.isRequired,
};

export default Button;

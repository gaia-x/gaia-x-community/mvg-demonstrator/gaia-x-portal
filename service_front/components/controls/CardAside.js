import React from "react";
import { any, string, bool } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  thumb: {
    background: ({ success }) =>
      success
        ? theme.palette.gradients.success
        : theme.palette.gradients.default,
    borderRadius: theme.shape.borderRadius,
    height: "100%",
  },
  backgroundImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundImage: "url(/gaia-x-tree.svg)",
    backgroundRepeat: "no-repeat",
    backgroundSize: ({ expanded }) => (expanded ? "100%" : "70%"),
    backgroundPosition: "right bottom",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
}));

const CardAside = ({ children, className, expanded, success }) => {
  const classes = useStyles({ expanded, success });
  return (
    <div className={clsx(classes.thumb, className)}>
      <div className={classes.backgroundImage}>{children}</div>
    </div>
  );
};

CardAside.propTypes = {
  children: any,
  className: string,
  expanded: any,
  success: bool,
};

export default CardAside;

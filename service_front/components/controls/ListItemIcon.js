import React from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { ListItemIcon as MuiListItemIcon } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.primary[200],
    minWidth: theme.spacing(4),
  },
}));

const ListItemIcon = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <MuiListItemIcon
      className={className}
      classes={{ root: classes.root }}
      {...rest}
    />
  );
};

ListItemIcon.propTypes = {
  className: string,
};

export default ListItemIcon;

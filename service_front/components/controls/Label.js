import React from "react";
import { string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  label: {
    fontSize: theme.typography.size.m,
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    textTransform: "uppercase",
    whiteSpace: "nowrap",
    flexBasis: "50%",
  },
}));

const Label = ({ children, className, htmlFor, style }) => {
  const classes = useStyles();
  return (
    <label
      className={clsx(classes.label, className)}
      htmlFor={htmlFor}
      style={style}
    >
      {children}
    </label>
  );
};

Label.propTypes = {
  children: string.isRequired,
  className: string,
  htmlFor: string,
};

export default Label;

import React, { forwardRef } from "react";
import { string } from "prop-types";
import { List as MuiList } from "@material-ui/core";

const List = forwardRef((props, ref) => {
  return <MuiList ref={ref} className={props.className} {...props} />;
});

List.propTypes = {
  className: string,
};

export default List;

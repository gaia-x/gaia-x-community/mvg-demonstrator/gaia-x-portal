import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useMenus, Link } from "docz";
import Card from "./Card";
import Typography from "./Typography";
import Grid from "./Grid";
const designPrinciplesImage = "images/gaia-x-design-principles.svg";
const wordingLabelingImage = "images/gaia-x-wording-labeling.svg";
const productImage = "images/gaia-x-product.svg";
const componentsImage = "images/gaia-x-components.svg";
const patternsImage = "images/gaia-x-patterns.svg";

const useStyles = makeStyles((theme) => ({
  section: {
    padding: theme.spacing(5, 25, 12, 12),
    margin: theme.spacing(5, 0),
    borderRadius: theme.shape.borderRadius,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "right bottom",
    backgroundSize: "50%",
    [theme.breakpoints.only("md")]: {
      padding: theme.spacing(5, 5, 6, 6),
    },
    [theme.breakpoints.only("xs")]: {
      padding: theme.spacing(5),
    },
  },
  card: {
    marginBottom: 0,
    height: theme.spacing(16),
    alignItems: "center",
    justifyContent: "center",
    "& h4": {
      textAlign: "center",
      transition: theme.transitions.create("all"),
    },
    "&:hover": {
      "& h4": {
        fontSize: theme.typography.size.l,
      },
    },
  },
}));

const Introduction = () => {
  const classes = useStyles();
  const theme = useTheme();
  const menus = useMenus();
  const designPrinciples = menus[1];
  const voiceTone = menus[2];
  const product = menus[3];
  const components = menus[4];
  const patterns = menus[5];
  return (
    <>
      <section
        className={classes.section}
        style={{
          backgroundColor: theme.palette.primary[50],
          backgroundImage: `url(${designPrinciplesImage})`,
        }}
      >
        <h2>{designPrinciples.name}</h2>
        <Grid container spacing={3}>
          {designPrinciples.menu.map((item) => {
            return (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Link to={item.route}>
                  <Card hasHover className={classes.card}>
                    <Typography variant="h4">{item.name}</Typography>
                  </Card>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{
          backgroundColor: theme.palette.neutral[400],
          backgroundImage: `url(${wordingLabelingImage})`,
        }}
      >
        <h2>{voiceTone.name}</h2>
        <Grid container spacing={3}>
          {voiceTone.menu.map((item) => {
            return (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Link to={item.route}>
                  <Card hasHover className={classes.card}>
                    <Typography variant="h4">{item.name}</Typography>
                  </Card>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{
          backgroundColor: theme.palette.secondary[200],
          backgroundImage: `url(${productImage})`,
        }}
      >
        <h2>{product.name}</h2>
        <Grid container spacing={3}>
          {product.menu.map((item) => {
            return (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Link to={item.route}>
                  <Card hasHover className={classes.card}>
                    <Typography variant="h4">{item.name}</Typography>
                  </Card>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{
          backgroundColor: theme.palette.neutral.main,
          backgroundImage: `url(${componentsImage})`,
        }}
      >
        <h2>{components.name}</h2>
        <Grid container spacing={3}>
          {components.menu.map((item) => {
            return (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Link to={item.route}>
                  <Card hasHover className={classes.card}>
                    <Typography variant="h4">{item.name}</Typography>
                  </Card>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </section>
      <section
        className={classes.section}
        style={{
          backgroundColor: theme.palette.secondary[100],
          backgroundImage: `url(${patternsImage})`,
        }}
      >
        <h2>{patterns.name}</h2>
        <Grid container spacing={3}>
          {patterns.menu.map((item) => {
            return (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Link to={item.route}>
                  <Card hasHover className={classes.card}>
                    <Typography variant="h4">{item.name}</Typography>
                  </Card>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </section>
    </>
  );
};

export default Introduction;

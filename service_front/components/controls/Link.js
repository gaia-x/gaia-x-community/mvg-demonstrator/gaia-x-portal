import React from "react";

import { Link as MuiLink } from "@material-ui/core";

const Link = (props) => {
  return (
    <MuiLink {...props} underline="none">
      {props.children}
    </MuiLink>
  );
};

export default Link;

import React, { forwardRef } from "react";
import { object, elementType, string, node, bool } from "prop-types";
import { Typography as MuiTypography } from "@material-ui/core";

const Typography = forwardRef((props, ref) => {
  return (
    <MuiTypography
      ref={ref}
      align={props.align}
      classes={props.classes}
      className={props.className}
      color={props.color}
      component={props.component}
      display={props.display}
      gutterBottom={props.gutterBottom}
      noWrap={props.noWrap}
      paragraph={props.paragraph}
      variant={props.variant}
    >
      {props.children}
    </MuiTypography>
  );
});

Typography.propTypes = {
  align: string,
  children: node,
  classes: object,
  className: string,
  color: string,
  component: elementType,
  display: string,
  gutterBottom: bool,
  noWrap: bool,
  paragraph: bool,
  variant: string,
};

Typography.defaultPropTypes = {
  align: "inherit",
  color: "initial",
  display: "initial",
  gutterBottom: false,
  noWrap: false,
  paragraph: false,
  variant: "body1",
};

export default Typography;

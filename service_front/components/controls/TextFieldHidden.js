import React from "react";
import { arrayOf, element, oneOfType, string } from "prop-types";
import TextField from "./TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    "& fieldset": {
      border: 0,
      "& legend": {
        height: 0,
        display: "none",
      },
    },
    flexBasis: "50%",
  },
});

const TextFieldHidden = (props) => {
  const classes = useStyles();

  return <TextField className={classes.root} {...props} />;
};

TextFieldHidden.propTypes = {
  children: oneOfType([arrayOf(element), element, string]),
};

export default TextFieldHidden;

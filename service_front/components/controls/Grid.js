import React from "react";
import { Grid as MuiGrid } from "@material-ui/core";

const Grid = (props) => <MuiGrid {...props}>{props.children}</MuiGrid>;

export default Grid;

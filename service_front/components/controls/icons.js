export const icons = [
    {
        key: "link",
    },
    {
        key: "node",
    },
    {
        key: "add-file",
    },
    {
        key: "bookmark",
    },
    {
        key: "cloud-check",
    },
    {
        key: "cloud-download",
    },
    {
        key: "cloud-sync",
    },
    {
        key: "cloud-upload",
    },
    {
        key: "cog",
    },
    {
        key: "database",
    },
    {
        key: "download",
    },
    {
        key: "empty-file",
    },
    {
        key: "files",
    },
    {
        key: "folder",
    },
    {
        key: "grid-alt",
    },
    {
        key: "map-marker",
    },
    {
        key: "package",
    },
    {
        key: "popup",
    },
    {
        key: "remove-file",
    },
    {
        key: "rocket",
    },
    {
        key: "save",
    },
    {
        key: "search",
    },
    {
        key: "shield",
    },
    {
        key: "upload",
    },
    {
        key: "warning",
    },
    {
        key: "eye-close",
    },
    {
        key: "eye",
    },
    {
        key: "add-user",
    },
    {
        key: "home",
    },
    {
        key: "medall",
    },
    {
        key: "user",
    },
    {
        key: "users",
    },
    {
        key: "camera",
    },
    {
        key: "stop",
    },
    {
        key: "volume",
    },
    {
        key: "volume-high",
    },
    {
        key: "provider",
    },
    {
        key: "checkbox-checked",
    },
    {
        key: "checkbox",
    },
    {
        key: "error-circle",
    },
    {
        key: "checkmark",
    },
    {
        key: "more-filled-vertical",
    },
    {
        key: "more-vertical",
    },
    {
        key: "plus-circle",
    },
    {
        key: "checkmark-circle",
    },
    {
        key: "circle-minus",
    },
    {
        key: "close",
    },
    {
        key: "cross-circle",
    },
    {
        key: "heart",
    },
    {
        key: "heart-filled",
    },
    {
        key: "more",
    },
    {
        key: "more-filled",
    },
    {
        key: "na",
    },
    {
        key: "plus",
    },
    {
        key: "protection",
    },
    {
        key: "question-circle",
    },
    {
        key: "star",
    },
    {
        key: "star-filled",
    },
    {
        key: "trash",
    },
    {
        key: "zoom-in",
    },
    {
        key: "zoom-out",
    },
    {
        key: "neutral",
    },
    {
        key: "sad",
    },
    {
        key: "smile",
    },
    {
        key: "certificate",
    },
    {
        key: "pencil",
    },
    {
        key: "pencil-alt",
    },
    {
        key: "cart",
    },
    {
        key: "cart-full",
    },
    {
        key: "shopping-basket",
    },
    {
        key: "chevron-down",
    },
    {
        key: "angle-double-down",
    },
    {
        key: "angle-double-left",
    },
    {
        key: "angle-double-right",
    },
    {
        key: "angle-double-up",
    },
    {
        key: "chevron-down-circle",
    },
    {
        key: "chevron-left",
    },
    {
        key: "chevron-left-circle",
    },
    {
        key: "chevron-right",
    },
    {
        key: "chevron-right-circle",
    },
    {
        key: "chevron-up",
    },
    {
        key: "chevron-up-circle",
    },
    {
        key: "enter",
    },
    {
        key: "exit",
    },
    {
        key: "exit-down",
    },
    {
        key: "exit-up",
    },
    {
        key: "radio-button",
    },
    {
        key: "dollar",
    },
    {
        key: "euro",
    },
    {
        key: "apartment",
    },
    {
        key: "comments",
    },
    {
        key: "comments-alt",
    },
    {
        key: "comments-reply",
    },
    {
        key: "envelope",
    },
    {
        key: "support",
    },
    {
        key: "bar-chart",
    },
    {
        key: "credit-cards",
    },
    {
        key: "funnel",
    },
    {
        key: "paper-clip",
    },
    {
        key: "world",
    },
    {
        key: "paypal",
    },
    {
        key: "paypal-original",
    },
    {
        key: "visa",
    },
];
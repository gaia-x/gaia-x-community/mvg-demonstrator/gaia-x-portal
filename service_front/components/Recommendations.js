import React from "react";
import Section from "./Section";
import ThumbCard from "./ThumbCard";
import { provider, services } from "../helpers/dummyData";
import { PrimaryButton, Grid, Link } from "./index";

const Recommendations = () => {
  const handleClick = () => {
    console.info("You clicked the Chip.");
  };

  return (
    <Section name="Recommendations">
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCard
            name={services[0].name}
            chipLabel={services[0].chipLabel}
            excerpt={services[0].excerpt}
            onClick={handleClick}
            kind="secondary"
            actions={
              // The Link should go to the services site with expanded service panel
              <Link to="/services">
                <PrimaryButton>Details</PrimaryButton>
              </Link>
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCard
            name={services[3].name}
            chipLabel={services[3].chipLabel}
            excerpt={services[3].excerpt}
            onClick={handleClick}
            kind="secondary"
            actions={
              // The Link should go to the services site with expanded service panel
              <Link to="/services">
                <PrimaryButton>Details</PrimaryButton>
              </Link>
            }
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4}>
          <ThumbCard
            name={provider[2].name}
            chipLabel={provider[2].chipLabel}
            excerpt={provider[2].excerpt}
            onClick={handleClick}
            kind="primary"
            isProvider
            actions={
              // The Link should go to the provider site with expanded provider panel
              <Link to="/provider">
                <PrimaryButton>Details</PrimaryButton>
              </Link>
            }
          />
        </Grid>
      </Grid>
    </Section>
  );
};

export default Recommendations;

import React from "react";
import { Grid, Label, Typography, Icon, makeStyles, Tooltip } from "./index";

const useStyles = makeStyles((theme) => ({
  contactContainer: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.neutral.light,
    borderRadius: theme.shape.borderRadius,
    padding: theme.spacing(1),
  },
  contactLabel: {
    color: theme.palette.primary[900],
  },
  contactContent: {
    color: theme.palette.primary[900],
    lineHeight: 1,
  },
  contactIcon: {
    color: theme.palette.primary[200],
    fontSize: theme.typography.size.xl,
    alignSelf: "flex-end",
  },
}));

const AgentCard = (agent) => {
  const classes = useStyles();

  return (
    <Grid item xs={12} sm={4}>
      <Tooltip title="The values in this element are randomly generated">
        <div className={classes.contactContainer}>
          <Label className={classes.contactLabel}>{agent.fullName}</Label>
          <Typography variant="body1" className={classes.contactContent}>
            {agent.fullName}
          </Typography>
          <Icon className={classes.contactIcon}>envelope</Icon>
          {/* {agent.mail ? (
            <Icon className={classes.contactIcon}>envelope</Icon>
          ) : null}
          {agent.phone ? (
            <Icon className={classes.contactIcon}>phone</Icon>
          ) : null} */}
        </div>
      </Tooltip>
    </Grid>
  );
};

export default AgentCard;

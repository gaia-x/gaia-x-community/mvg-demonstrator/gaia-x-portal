import React, { useState } from "react";
import { string, bool, func, any } from "prop-types";
import clsx from "clsx";
import {
  Card,
  Avatar,
  Checkbox,
  Chip,
  Label,
  PrimaryButton,
  Grid,
  Divider,
  Tooltip,
  Typography,
  Collapse,
  makeStyles,
  ExternalLink,
  Icon,
  CircularProgress,
  SecondaryButton,
} from "./index";
const fallbackLogo = "/images/gaia-x-tree.svg";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    backgroundImage: ({ bgImage }) => `url(${bgImage})`,
    backgroundSize: ({ expanded }) => (expanded ? "50%" : "0%"),
    backgroundPosition: "bottom right",
    backgroundRepeat: "no-repeat",
    transition: theme.transitions.create("all"),
    position: "relative",
  },
  short: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  long: {
    marginLeft: `-${theme.spacing(3)}px`,
    width: `calc(100% + ${theme.spacing(6)}px)`,
  },
  divider: {
    marginTop: theme.spacing(3),
  },
  compositeDivider1: {
    backgroundColor: "#B3C4C8",
    width: 3,
    height: 102,
    marginTop: "-24px",
    left: 9,
    position: "absolute",
  },
  compositeDivider2: {
    backgroundColor: "#B3C4C8",
    width: 3,
    height: 102,
    marginTop: "-24px",
    left: 3,
    position: "absolute",
  },
  flex: {
    display: "flex",
    alignItems: "center",
  },
  margined: {
    marginRight: theme.spacing(3),
  },
  column: {
    flexDirection: "column",
  },
  itemContainer: {
    display: "flex",
    [theme.breakpoints.down("xs")]: {
      "& div$item:first-child": {
        display: "none",
      },
      "& div$item:nth-child(2)": {
        display: "none",
      },
      "&  div$item:last-child": {
        display: "none",
      },
    },
    [theme.breakpoints.up("sm")]: {
      "& div$item:first-child": {
        display: "flex",
      },
      "& div$item:nth-child(2)": {
        display: "none",
      },
      "& div$item:last-child": {
        display: "none",
      },
    },
    [theme.breakpoints.up("md")]: {
      "& div$item:first-child": {
        display: "flex",
      },
      "& div$item:nth-child(2)": {
        display: "none",
      },
      "& div$item:last-child": {
        display: "none",
      },
    },
    [theme.breakpoints.up("lg")]: {
      "& div$item:first-child": {
        display: "flex",
      },
      "& div$item:nth-child(2)": {
        display: "flex",
      },
      "& div$item:last-child": {
        display: "flex",
      },
    },
  },
  item: {
    marginRight: theme.spacing(3),
    "& label": {
      fontSize: theme.typography.size.s,
      lineHeight: "1.1",
    },
  },
  itemIcon: {
    color: theme.palette.primary[200],
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(0.5),
    fontSize: theme.typography.size.m,
  },
  providerName: {
    minWidth: theme.spacing(66),
    fontSize: theme.typography.size.l,
    maxWidth: theme.spacing(66),
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      maxWidth: theme.spacing(50),
      width: theme.spacing(50),
    },
  },
  serviceName: {
    minWidth: theme.spacing(66),
    maxWidth: theme.spacing(66),
    whiteSpace: "nowrap",
    marginTop: `-${theme.spacing(1)}px`,
    marginBottom: theme.spacing(0.5),
    textOverflow: "ellipsis",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      maxWidth: theme.spacing(50),
      width: theme.spacing(50),
    },
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
    [theme.breakpoints.down("xs")]: {
      margin: "0 !important",
    },
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  detailsLabel: {
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  loading: {
    marginRight: "5px",
    height: "20px !important",
    width: "20px !important",
  },
  flag: {
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    height: theme.spacing(2),
    width: theme.spacing(2),
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(1),
    boxShadow: theme.dimensions.shadowCard,
  },
}));

const ListItemCard = ({
  children,
  name,
  chipLabel,
  apiType,
  reporting,
  stack,
  security,
  location,
  onClick,
  actions,
  isProvider,
  variant,
  href,
  logo,
  availabilty,
  bgImage,
  loading,
  error,
  initiallyExpanded = false,
}) => {
  const [expanded, setExpanded] = useState(initiallyExpanded);
  const classes = useStyles({
    expanded,
    bgImage,
  });

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const getFlag = (location) => {
    switch (location) {
      case "France":
        return "/images/french-flag.svg";
      case "Germany":
        return "/images/german-flag.svg";
      case "Ireland":
        return "/images/irish-flag.svg";
      case "Spain":
        return "/images/spanish-flag.svg";
      case "Sweden":
        return "/images/swedish-flag.svg";
      case "Czech Republic":
        return "/images/czech-republic-flag.svg";
      case "Austria":
        return "/images/austrian-flag.svg";
      case "United Kingdom":
        return "/images/english-flag.svg";
      default:
        return null;
    }
  };

  return (
    <Card actions={actions} className={classes.card}>
      {variant === "composite" && (
        <>
          <Divider
            orientation="vertical"
            flexItem
            className={classes.compositeDivider1}
          />
          <Divider
            orientation="vertical"
            flexItem
            className={classes.compositeDivider2}
          />
        </>
      )}
      <div className={classes.short}>
        <div className={classes.flex}>
          <div className={classes.flex}>
            {/* {!isProvider && (
              <Tooltip title="Select to compare">
                <Checkbox className={classes.margined} />
              </Tooltip>
            )} */}
            <Avatar
              src={!!logo ? logo : fallbackLogo}
              alt={name}
              className={classes.margined}
              boxShadow
              size="large"
            />
            {isProvider ? (
              <div className={classes.item}>
                <Typography variant="h4" className={classes.providerName}>
                  {name}
                </Typography>
                {/* <ExternalLink href={href} target="_blank" rel="noreferrer">
                <Chip label={chipLabel} size="small" hasIcon />
              </ExternalLink> */}
              </div>
            ) : (
              <div className={classes.item}>
                <Typography variant="h4" className={classes.serviceName}>
                  {name}
                </Typography>
                <ExternalLink href={href} target="_blank" rel="noreferrer">
                  <Chip label={chipLabel} size="small" hasIcon />
                </ExternalLink>
              </div>
            )}
          </div>
          {isProvider ? (
            <div className={classes.itemContainer}>
              {/* <div className={classes.item}>
                <Tooltip title="Certificates">
                  <Icon className={classes.itemIcon}>medall</Icon>
                </Tooltip>
                <div className={classes.column}>
                  <Label>Certificates</Label>
                  <Typography>DIN EN ISO 9001</Typography>
                </div>
              </div>
              <div className={classes.item}>
                <Tooltip title="Reporting">
                  <Icon className={classes.itemIcon}>bar-chart</Icon>
                </Tooltip>
                <div className={classes.column}>
                  <Label>Reporting</Label>
                  <Typography>{reporting}</Typography>
                </div>
              </div> */}
              {location && (
                <div className={classes.item}>
                  {/* <Tooltip title="Location">
                    <Icon className={classes.itemIcon}>map-marker</Icon>
                  </Tooltip> */}
                  <div className={classes.column}>
                    <Label>Location</Label>
                    <div className={classes.flex}>
                      <div
                        className={classes.flag}
                        role="img"
                        alt="Location"
                        style={{
                          backgroundImage: `url(${getFlag(location)})`,
                        }}
                      />
                      <Typography variant="body2">{location}</Typography>
                    </div>
                  </div>
                </div>
              )}
            </div>
          ) : (
            <div className={classes.itemContainer}>
              {apiType && (
                <div className={classes.item}>
                  <Tooltip title="API">
                    <Icon className={classes.itemIcon}>code</Icon>
                  </Tooltip>

                  <div className={classes.column}>
                    <Label>API</Label>
                    <Typography>{apiType}</Typography>
                  </div>
                </div>
              )}
              {/* <div className={classes.item}>
                <Tooltip title="Security">
                  <Icon className={classes.itemIcon}>protection</Icon>
                </Tooltip>
                <div className={classes.column}>
                  <Label>Security</Label>
                  <Typography>{security}</Typography>
                </div>
              </div>
              <div className={classes.item}>
                <Tooltip title="Availabilty">
                  <Icon className={classes.itemIcon}>checkmark-circle</Icon>
                </Tooltip>
                <div className={classes.column}>
                  <Label>Availabilty</Label>
                  <Typography>{availabilty}</Typography>
                </div>
              </div> */}
            </div>
          )}
        </div>
        {(!loading && !error && (
          <PrimaryButton onClick={handleExpandClick} aria-expanded={expanded}>
            <Icon
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
            >
              chevron-down
            </Icon>
            <span className={classes.detailsLabel}>Details</span>
          </PrimaryButton>
        )) ||
          (!loading && error && (
            <Tooltip title="Failed to load more Informations">
              <SecondaryButton aria-expanded={expanded}>
                <Icon>close</Icon>
                <span className={classes.detailsLabel}>Error</span>
              </SecondaryButton>
            </Tooltip>
          )) || <CircularProgress className={classes.loading} />}
      </div>
      <Collapse
        in={!loading && expanded}
        timeout="auto"
        unmountOnExit
        className={classes.long}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Divider className={classes.divider} />
            {children}
          </Grid>
        </Grid>
      </Collapse>
    </Card>
  );
};

ListItemCard.propTypes = {
  name: string,
  stack: string,
  security: string,
  location: string,
  onClick: func,
  isProvider: bool,
  actions: any,
  variant: string,
  logo: string,
  href: string,
  availabilty: string,
  children: any,
};

ListItemCard.defaultProps = {
  isProvider: false,
};

export default ListItemCard;

import React from "react";
import { any, string, node, bool, func } from "prop-types";
import {
  Card,
  Chip,
  Typography,
  Tooltip,
  Icon,
  IconButton,
  makeStyles,
} from "./index";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "space-between",
    padding: theme.spacing(2),
    background: `linear-gradient(to bottom right, ${theme.palette.neutral[50]}, ${theme.palette.neutral[100]})`,
    position: "relative",
    margin: 0,
    minHeight: theme.spacing(50),
  },
  flex: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(2),
  },
  icon: {
    height: theme.spacing(7),
    width: theme.spacing(7),
    marginBottom: theme.spacing(3),
    "& svg": {
      fontSize: theme.spacing(7),
      fill: theme.palette.warning.main,
      marginRight: theme.spacing(2),
    },
  },
  excerpt: {
    color: theme.palette.neutral[1000],
    marginBottom: theme.spacing(2),
    overflow: "hidden",
    display: "-webkit-box",
    "-webkit-line-clamp": 5,
    "-webkit-box-orient": "vertical",
  },
  chipContainer: {
    marginBottom: theme.spacing(2),
    display: "flex",
  },
  thumb: {
    borderRadius: theme.shape.borderRadius,
    height: theme.spacing(12),
    marginBottom: theme.spacing(2),
  },
  backgroundImage: {
    padding: theme.spacing(3),
    borderRadius: theme.shape.borderRadius,
    height: "100%",
    backgroundRepeat: "no-repeat",
    backgroundImage: ({ marketingImage }) => `url(${marketingImage})`,
    backgroundSize: "cover",
    backgroundPosition: "top center",
    transition: theme.transitions.create("background-size", {
      duration: theme.transitions.duration.complex,
      easing: theme.transitions.easing.easeIn,
    }),
  },
  edit: {
    position: "absolute",
    top: theme.spacing(3),
    right: theme.spacing(3),
    backgroundColor: theme.palette.primary[50],
    transition: theme.transitions.create("all"),
    "&:hover": {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.common.white,
    },
  },
  chip: {
    maxWidth: theme.spacing(28),
    textOverflow: "ellipsis",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      maxWidth: theme.spacing(11),
      textOverflow: "ellipsis",
      overflow: "hidden",
    },
  },
}));

const SetCard = ({
  icon,
  name,
  chipLabel,
  excerpt,
  onEdit,
  marketingImage,
  hasShadow,
  actions,
  builderMode,
  searchMode,
  onSearch,
  ...rest
}) => {
  const classes = useStyles({ marketingImage });
  return (
    <Card
      {...rest}
      actions={actions}
      className={classes.card}
      hasShadow={hasShadow}
    >
      <div>
        <div className={classes.thumb}>
          <div className={classes.backgroundImage}>
            {builderMode && (
              <IconButton
                className={classes.edit}
                size="small"
                color="secondary"
                onClick={onEdit}
              >
                <Icon>pencil</Icon>
              </IconButton>
            )}
            {searchMode && (
              <IconButton
                className={classes.edit}
                size="small"
                color="secondary"
                onClick={onSearch}
              >
                <Icon>search</Icon>
              </IconButton>
            )}
          </div>
        </div>
        <div className={classes.flex}>
          <div className={classes.item}>
            <Typography variant="h4">{name}</Typography>
            {chipLabel && (
              <Tooltip title={chipLabel}>
                <div>
                  <Chip
                    label={chipLabel}
                    size="small"
                    hasIcon
                    className={classes.chip}
                  />
                </div>
              </Tooltip>
            )}
          </div>
        </div>
        <div>
          <Typography variant="body2" className={classes.excerpt}>
            {excerpt}
          </Typography>
        </div>
      </div>
    </Card>
  );
};

SetCard.propTypes = {
  icon: node,
  chipLabel: string,
  excerpt: string,
  onEdit: func,
  actions: any,
  builderMode: bool,
};

SetCard.defaultProps = {
  builderMode: false,
};

export default SetCard;

const serviceImage = "/images/service-image.jpg";
const dataImage = "/images/data-image.jpg";
const providerImage = "/images/provider-image.jpg";
const storageImage = "/images/storageImage.jpg";
const germanFlag = "/images/german-flag.svg";
const englishFlag = "/images/english-flag.svg";
const frenchFlag = "/images/french-flag.svg";
const spanishFlag = "/images/spanish-flag.svg";
const bgImage = "/images/storage.svg";
const providerImg = "/images/provider.svg";
const citySensor = "/images/citySensor.jpg";
const radiology = "/images/radiology.jpg";
const kubernetes = "/images/k8-cluster.jpg";
const storage = "/images/storage.jpg";

export const services = [
  {
    id: 1,
    name: "S3 Storage",
    chipLabel: "Provider X",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Stuttgart",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: storageImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "95%",
    flag: germanFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "basic",
    logo:
      "https://www.godeltech.com/wp-content/uploads/sites/2/2016/05/Cloud-Technology-Solutions-2.png",
  },
  {
    id: 2,
    name: "S3 Storage Plus",
    chipLabel: "Provider Y",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: storageImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "99%",
    flag: germanFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "basic",
    logo:
      "https://static.vecteezy.com/system/resources/previews/000/613/043/non_2x/vector-modern-letter-a-technology-business-logo-design-concept-template.jpg",
  },
  {
    id: 3,
    name: "AI Service",
    chipLabel: "Provider W",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Paris",
    price: {
      amount: "180,96€",
      per: " / Month",
    },
    marketingImage: serviceImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "85%",
    flag: frenchFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "composite",
    logo:
      "https://i.pinimg.com/736x/eb/a7/d9/eba7d9c0e760dae8737362bb1d257ef2.jpg",
  },
  {
    id: 4,
    name: "AI Service Plus",
    chipLabel: "Provider A",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "London",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: serviceImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "95%",
    flag: englishFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "composite",
    logo: "https://logopond.com/logos/6ec8bc6e13fd3c7b156ec64bcb7ea7d5.png",
  },
  {
    id: 5,
    name: "S3 Storage Plus",
    chipLabel: "Provider Z",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: serviceImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "98%",
    flag: germanFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "basic",
    logo:
      "https://assets.materialup.com/uploads/ff730455-770b-4609-b2c9-a9432e284ba6/preview.jpg",
  },
  {
    id: 6,
    name: "Kubernetes Cluster",
    chipLabel: "Provider F",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: kubernetes,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "95%",
    flag: germanFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "basic",
    logo:
      "https://cdn.dribbble.com/users/1298922/screenshots/6653885/01_preview1.jpg",
  },
  {
    id: 7,
    name: "Compute",
    chipLabel: "Provider F",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: storage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "92%",
    flag: germanFlag,
    bgImage: bgImage,
    screenshots: [
      "https://cdn.dribbble.com/users/427857/screenshots/14356166/media/98df10696e2136ba56069ee7503b9070.png",
      "https://cdn.dribbble.com/users/427857/screenshots/14347617/media/4014061bc62b5a083d44ab3da78db6d5.png",
    ],
    variant: "basic",
    logo:
      "https://image.freepik.com/free-vector/colorful-p-letter-photography-logo_1061-554.jpg",
  },
];

export const data = [
  {
    id: 1,
    name: "Health Plan Enrollment 1",
    chipLabel: "Provider X",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Berlin",
    availabilty: "92%",
    price: {
      amount: "180,96€",
      per: " / Month",
    },
    marketingImage: dataImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "95%",
    flag: germanFlag,
    bgImage: bgImage,
    logo:
      "https://cdn5.f-cdn.com/contestentries/162862/14037053/54ee0fa5250d9_thumb900.jpg",
  },
  {
    id: 2,
    name: "Health Plan Enrollment 2",
    chipLabel: "Provider Y",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Marseille",
    price: {
      amount: "180,96€",
      per: " / Month",
    },
    marketingImage: dataImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "99%",
    flag: frenchFlag,
    bgImage: bgImage,
    logo:
      "https://static.vecteezy.com/system/resources/previews/000/613/043/non_2x/vector-modern-letter-a-technology-business-logo-design-concept-template.jpg",
  },
  {
    id: 3,
    name: "Health Plan Enrollment 3",
    chipLabel: "Provider W",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Toulouse",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: dataImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "91%",
    flag: frenchFlag,
    bgImage: bgImage,
    logo:
      "https://i.pinimg.com/originals/f6/71/9e/f6719e05240f543fc59736915d49ec37.jpg",
  },
  {
    id: 4,
    name: "Health Plan Enrollment 4",
    chipLabel: "Provider A",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "London",
    price: {
      amount: "180,96€",
      per: " / Month",
    },
    marketingImage: dataImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "97%",
    flag: englishFlag,
    bgImage: bgImage,
    logo:
      "https://3.bp.blogspot.com/-VERMpooajNg/WnNWjwXW24I/AAAAAAAAAw0/EtbDJjBMWmkDe3BvsYdhTcoqpxmxDxgcwCLcBGAs/s1600/technology-logo.jpg",
  },
  {
    id: 5,
    name: "Health Plan Enrollment 5",
    chipLabel: "Provider Z",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Hannover",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: dataImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "96%",
    flag: germanFlag,
    bgImage: bgImage,
    logo:
      "https://www.logoground.com/uploads/201634982016-02-013504286logo_template_randomtarget.jpg",
  },
  {
    id: 6,
    name: "City Sensor Data",
    chipLabel: "Provider F",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: citySensor,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "92%",
    flag: germanFlag,
    bgImage: bgImage,
    logo:
      "https://www.logoground.com/uploads/201634982016-02-013602676logo_template_vibebusiness.jpg",
  },
  {
    id: 7,
    name: "87K Radiology Pictures",
    chipLabel: "Provider F",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    features: "10GB",
    stack: "Openstack",
    security: "ACL",
    location: "Frankfurt",
    price: {
      amount: "99€",
      per: " / Month",
    },
    marketingImage: radiology,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    availabilty: "94%",
    flag: germanFlag,
    bgImage: bgImage,
    logo: "https://creator.design/_logos/65/logo-65_07.png",
  },
];

export const provider = [
  {
    id: 1,
    name: "Provider Y",
    chipLabel: "www.provider-y.com",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    certificates: "DIN EN ISO 9001",
    reporting: "Uptime",
    contact: {
      commercialEmailAddress: "info@provider-y.com",
      commercialPhoneNumber: "+49308833255",
      technicalEmailAddress: "sup@provider-y.com",
      technicalPhoneNumber: "+49308833266",
    },
    location: "Frankfurt",
    marketingImage: providerImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    memberSince: "01.11.2020",
    flag: germanFlag,
    bgImage: providerImg,
    logo: "https://www.infopulse.com/files/images/it-company-logo.png",
  },
  {
    id: 2,
    name: "Provider A",
    chipLabel: "www.provider-a.com",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    certificates: "DIN EN ISO 9001",
    reporting: "Uptime",
    contact: {
      commercialEmailAddress: "info@provider-a.com",
      commercialPhoneNumber: "+49308833255",
      technicalEmailAddress: "sup@provider-a.com",
      technicalPhoneNumber: "+49308833266",
    },
    location: "Valencia",
    marketingImage: providerImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    memberSince: "01.11.2020",

    flag: spanishFlag,
    bgImage: providerImg,
    logo:
      "https://i.pinimg.com/originals/58/04/42/58044262d2ef49000c8ed4174dd3c5c4.png",
  },
  {
    id: 3,
    name: "Provider W",
    chipLabel: "www.provider-w.com",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    certificates: "DIN EN ISO 9001",
    reporting: "Uptime",
    contact: {
      commercialEmailAddress: "info@provider-w.com",
      commercialPhoneNumber: "+49308833255",
      technicalEmailAddress: "sup@provider-w.com",
      technicalPhoneNumber: "+49308833266",
    },
    location: "Frankfurt",
    marketingImage: providerImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    memberSince: "01.11.2020",

    flag: germanFlag,
    bgImage: providerImg,
    logo:
      "https://www.tutorialchip.com/wp-content/uploads/2012/08/TechnoInsight.jpg",
  },
  {
    id: 4,
    name: "Provider O",
    chipLabel: "www.provider-o.com",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    certificates: "DIN EN ISO 9001",
    reporting: "Uptime",
    contact: {
      commercialEmailAddress: "info@provider-o.com",
      commercialPhoneNumber: "+49308833255",
      technicalEmailAddress: "sup@provider-o.com",
      technicalPhoneNumber: "+49308833266",
    },
    location: "Bilbao",
    marketingImage: providerImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    memberSince: "01.11.2020",

    flag: spanishFlag,
    bgImage: providerImg,
    logo:
      "https://cdn3.f-cdn.com/contestentries/1141729/11122428/59d5ddfa85171_thumb900.jpg",
  },
  {
    id: 5,
    name: "Provider K",
    chipLabel: "www.provider-k.com",
    excerpt:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ",
    certificates: "DIN EN ISO 9001",
    reporting: "Uptime",
    contact: {
      commercialEmailAddress: "info@provider-k.com",
      commercialPhoneNumber: "+49308833255",
      technicalEmailAddress: "sup@provider-k.com",
      technicalPhoneNumber: "+49308833266",
    },
    location: "Madrid",
    marketingImage: providerImage,
    lastUpdated: "15.09.2020, 14:36 Uhr",
    memberSince: "01.11.2020",
    flag: spanishFlag,
    bgImage: providerImg,
    logo:
      "https://d2slcw3kip6qmk.cloudfront.net/marketing/blogs/press/tech-company-branding-expert-tips/image07.png",
  },
];

export const account = {
  username: "Marion Schwertner",
  companyName: "Krankenhaus GmbH",
  commercialRegister: "HRB 12345, Amtsgericht Darmstadt, DE",
  registeredAddress: {
    streetNumber: "Savignyplatz 3",
    zipCity: "10623 Darmstadt",
    country: "Germany",
  },
  webAddress: "www.krankenhaus.de",
  individualContact: {
    commercialEmailAddress: "info@krankenhaus.de",
    commercialPhoneNumber: "+49308833255",
    technicalEmailAddress: "sup@krankenhaus.de",
    technicalPhoneNumber: "+49308833266",
  },
  certifications: "",
  alias: "",
  localAttestation: "",
  transparencyRegister: "",
  dunsNumber: "",
  legalEntityIdentifier: "",
  DataProviderOfficer: {
    name: "Pierre Michaux",
    role: "Data Provider Officer",
    company: "Data Provider GbR",
    address: {
      streetNumber: "Bleibtreustraße 45",
      zipCity: "10623 Berlin",
      country: "Germany",
      emailAddress: "pierre.michaux@data.com",
      phoneNumber: "+49308833233",
    },
  },
  vatNumber: "DE123456789",
};

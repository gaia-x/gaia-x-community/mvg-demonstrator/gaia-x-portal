import React, { useState } from "react";
import { Container, Chip, makeStyles } from "../components";
import LoginForm from "../components/LoginForm";
const europe = "/images/gaia-x-europe.jpg";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    marginBottom: `-${theme.spacing(12)}px`,
    overflow: "hidden",
    background: `url(${europe})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    position: "relative",
  },
}));

const Login = () => {
  const classes = useStyles();
  const [state, setState] = useState({
    checked: false,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <Container
      maxWidth={false}
      disableGutters
      component="article"
      className={classes.container}
    >
      <LoginForm
        onChangeTextfield={() => void 0}
        onChangePassword={() => void 0}
        onClickPrimaryButton={() => void 0}
        checked={state.checked}
        onChangeCheckbox={handleChange}
        forgotPasswordLink="/"
        registerLink="/register"
      />
      <Chip label="FAQ & Support" onClick={() => void 0} />
    </Container>
  );
};

export default Login;

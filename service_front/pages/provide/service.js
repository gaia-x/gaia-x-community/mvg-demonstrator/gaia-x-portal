import React, { useState } from "react";
import {
  Container,
  Card,
  CardAside,
  List,
  ListItem,
  Label,
  TextFieldHidden,
  Grid,
  PrimaryButton,
  SecondaryButton,
  makeStyles,
  CardParagraph,
  Icon,
  Step,
  Stepper,
  StepButton,
  Typography,
  Switch,
  StepLabel,
} from "../../components";

import Section from "../../components/Section";

const useStyles = makeStyles((theme) => ({
  card: {
    flexDirection: "column",
    position: "relative",
  },
  cardAside: {
    height: theme.spacing(13),
    "& > div": {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      backgroundSize: "12%",
    },
  },
  serviceIcon: {
    fontSize: theme.typography.size.xxl,
    color: theme.palette.primary[200],
    marginRight: theme.spacing(1),
  },
  hiddenInput: {
    display: "none",
  },
  imageIcon: {
    bottom: theme.spacing(6),
    position: "absolute",
    zIndex: 2,
    "& i": { color: "white" },
  },
  list: {
    paddingTop: 0,
    paddingBottom: theme.spacing(3),
  },
  listItem: {
    width: `calc(100% + ${theme.spacing(6)}px)`,
    marginLeft: `-${theme.spacing(3)}px`,
  },
  mandatory: {
    float: "right",
  },
}));

function getSteps() {
  return [1, 2, 3];
}

function getStepContent(step) {
  const classes = useStyles();

  const [state, setState] = useState({
    checkedA: true,
    checkedB: true,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  switch (step) {
    case 0:
      return (
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <CardParagraph margin={false}>
              Please upload or enter your Service Self-Description.
            </CardParagraph>
          </Grid>
          <Grid item xs={2}>
            <PrimaryButton>
              <Icon>upload</Icon>
              Upload
            </PrimaryButton>
          </Grid>
        </Grid>
      );
    case 1:
      return (
        <List className={classes.list}>
          <div className={classes.mandatory}>
            <Label>Show mandatory only</Label>
            <Switch
              checked={state.checkedB}
              onChange={handleChange}
              name="checkedB"
            />
          </div>
          {/* Hier müssten dann die Felder dynamisch reingeladen werden. */}
          <ListItem className={classes.listItem}>
            <Label htmlFor="label">Label</Label>
            <TextFieldHidden id="label" onChange={() => void 0} value="value" />
          </ListItem>
        </List>
      );
    default:
      return "Unknown step";
  }
}

const ProvideService = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [completed, setCompleted] = useState({});
  const steps = getSteps();

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  return (
    <Container maxWidth="md" component="article">
      <Section name="Provide Service">
        <Card
          actions={
            <Grid container spacing={1} justify="flex-end">
              {allStepsCompleted() ? (
                <Grid item>
                  <SecondaryButton onClick={handleReset}>Reset</SecondaryButton>
                </Grid>
              ) : (
                <>
                  <Grid item>
                    <SecondaryButton
                      disabled={activeStep === 0}
                      onClick={handleBack}
                    >
                      Back
                    </SecondaryButton>
                  </Grid>
                  <Grid item>
                    <PrimaryButton onClick={handleNext}>Next</PrimaryButton>
                  </Grid>
                  {activeStep !== steps.length &&
                    (completed[activeStep] ? (
                      <Grid item>
                        <PrimaryButton disabled>Completed</PrimaryButton>
                      </Grid>
                    ) : (
                      <Grid item>
                        <PrimaryButton onClick={handleComplete}>
                          {completedSteps() === totalSteps() - 1
                            ? "Finish"
                            : "Complete Step"}
                        </PrimaryButton>
                      </Grid>
                    ))}
                </>
              )}
            </Grid>
          }
          className={classes.card}
        >
          <Grid container spacing={3} alignItems="center">
            <Grid item xs={12}>
              <CardAside className={classes.cardAside}>
                <Icon className={classes.serviceIcon}>cog</Icon>
                <Typography variant="h1" component="h2">
                  Service Registration
                </Typography>
              </CardAside>
            </Grid>
            <Grid item xs={12}>
              <Stepper nonLinear activeStep={activeStep}>
                {steps.map((label, index) => (
                  <Step key={label}>
                    <StepButton
                      disableRipple
                      onClick={handleStep(index)}
                      completed={completed[index]}
                    >
                      <StepLabel />
                    </StepButton>
                  </Step>
                ))}
              </Stepper>
            </Grid>
          </Grid>
          {getStepContent(activeStep)}
        </Card>
      </Section>
    </Container>
  );
};

export default ProvideService;

import React from "react";
import { Container, Typography } from "../components";

import Portal from "../components/Portal";

const Marketplace = () => (
  <Portal>
    <Container maxWidth="md" component="article">
      <Typography variant="h1">Marketplace</Typography>
    </Container>
  </Portal>
);

export default Marketplace;

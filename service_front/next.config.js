module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    config.module.rules.push({
      loader: require.resolve("file-loader"),
      // Exclude `js` files to keep "css" loader working as it injects
      // its runtime that would otherwise be processed through "file" loader.
      // Also exclude `html` and `json` extensions so they get processed
      // by webpacks internal loaders.
      include: [/\.(woff)$/],
      options: {
        name: "static/media/[name].[hash:8].[ext]",
      },
    });
    // Important: return the modified config
    return config;
  },
};

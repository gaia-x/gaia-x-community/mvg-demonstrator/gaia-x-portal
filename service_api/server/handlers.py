import tornado.options
from tornado.web import RequestHandler, HTTPError
import http
from tornado import template
import logging
import os
import json
import functools
from marshmallow import Schema, fields


logger = logging.getLogger("tornado.application")


def return_json(f):
    @functools.wraps(f)
    async def wrapper(reqhandler, *args, **kwds):        
        reqhandler.set_header('Content-Type', 'application/json')
        data = await f(reqhandler, *args, **kwds)
        reqhandler.write(json.dumps(data))
    return wrapper


class BaseHandler(RequestHandler):
    def set_default_headers(self):
       self.set_header("Access-Control-Allow-Origin", "*")
       self.set_header('Access-Control-Allow-Methods', ','.join(['GET', 'OPTIONS', 'POST']))

    def options(self):
        self.set_status(204)
        self.finish()

    def write_error(self, status_code, **kwargs):
        if 'exc_info' not in kwargs:
            return
        exc_type, exc_value, exc_traceback = kwargs["exc_info"]
        msg = str(exc_value)
        if type(exc_type) == HTTPError:
            msg = exc_value.reason
        if hasattr(exc_value, 'message'):
          msg = exc_value.message
        logger.warning(msg)
        self.write(msg)  # return custom error message in the body


class InfoSchema(Schema):
    version = fields.String()


class InfoHandler(BaseHandler):
    @return_json
    async def get(self):
        """---
description: Server information
responses:
  200:
    description: return map of server properties
    content:
      application/json:
        schema: InfoSchema
"""
        data = {"version": self.application.settings['version']}
        return InfoSchema().dump(data)


class SpecHandler(BaseHandler):
    async def get(self):
        """---
description: OpenAPI 3 specification
responses:
  200:
    description: return OpenAPI3 YAML
    content:
      text/x-yaml:
        schema:
          type: string
"""
        self.set_header('Content-Type', 'text/x-yaml')
        self.write(self.application.settings['spec'].to_yaml())

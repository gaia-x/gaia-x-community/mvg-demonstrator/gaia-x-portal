import asyncio
from functools import partial

from neo4j import GraphDatabase
import logging

logger = logging.getLogger("tornado.application")
logger.setLevel(logging.DEBUG)


class GraphDB:
    def __init__(self, address='bolt://localhost:7687', username='neo4j', password='admin', encrypted=False):
        self._driver = GraphDatabase.driver(address, auth=(username, password), encrypted=encrypted)

    def close(self):
        self._driver.close()

    async def run(self, query, **kwargs):
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, partial(self._sync_run, query=query, **kwargs))

    def _sync_run(self, query, **kwargs):
        logger.debug("QUERY: {}".format(query))
        def _run(tx, query, kwargs):
            return tx.run(query, kwargs)
        with self._driver.session() as session:
            return session.read_transaction(_run, query, kwargs)

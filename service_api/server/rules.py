import json
from json import JSONEncoder, JSONDecodeError
import logging
from collections import namedtuple
from typing import List, Dict


logger = logging.getLogger("tornado.application")


def expression_decoder(exp):
    def _decoder(obj):
        if 'or' in obj:
            return Or(*obj['or'])
        if 'and' in obj:
            return And(*obj['and'])
        if 'key' in obj:
            return Attribute(**obj)
        return Relation(**obj)
    try:
        res = json.loads(exp, object_hook=_decoder)
    except TypeError as ex:
        raise JSONDecodeError("Invalid type: '{}'".format(exp), exp, 0)
    if not (isinstance(res, Attribute) or isinstance(res, Relation) or isinstance(res, __Logic)):
        raise JSONDecodeError("invalid expression: {}".format(exp), exp, 0)
    return res


async def check_node_label(db, label):
    res = await db.run("MATCH (n) UNWIND LABELS(n) AS label RETURN DISTINCT label")
    labels = map(lambda e: e['label'], res)
    assert label in labels, 'expecting a valid node label ({})'.format(label)


async def check_node_attribute(db, label, attribute):
    res = await db.run("MATCH (n:{label!s}) UNWIND KEYS(n) AS key RETURN DISTINCT key".format(label=label))
    keys = map(lambda e: e['key'], res)
    assert attribute in keys, 'expecting a valid attribute ({attribute}) for label {label}'.format(label=label, attribute=attribute)


async def check_relation_type(db, relation):
    res = await db.run("MATCH ()-[r]->() RETURN DISTINCT TYPE(r) AS rel")
    relations = map(lambda e: e['rel'], res)
    assert relation in relations, 'expecting a valid relation ({relation})'.format(relation=relation)


async def check_node_id(db, id):
    res = await db.run("MATCH (a) WHERE ID(a) = {:d} RETURN COUNT(a) AS count".format(id))
    assert 1 == res.single()['count']


class Node(dict):
    def __init__(self, id: str, labels: List[str] , properties: Dict[str, str]):
        dict.__init__(self, id=id, labels=labels, properties=properties)

    def __hash__(self):
        return self['id']


class Relation:
    def __init__(self, node, verb, value, neg=False, **kwargs):
        self.node, self.verb, self.value, self.neg = node, verb, value, bool(neg)
        for attr in ['node', 'verb', 'value']:
            if not isinstance(self.__getattribute__(attr), str):
                raise JSONDecodeError("'{}' must be a string".format(attr), attr, 0)
        if len(kwargs):
            raise JSONDecodeError("got extra parameter {}".format(kwargs), json.dumps(kwargs), 0)
    
    def __eq__(self, other):
        return self.node == other.node and self.verb == other.verb and self.value == other.value and self.neg == other.neg

    async def run(self, db):
        await check_node_label(db, self.node)
        await check_relation_type(db, self.verb)
        query = "MATCH p=((node:{node!s})-[*]->(dst)) WHERE length(p) < 10 AND TYPE(LAST(RELATIONSHIPS(p))) = '{verb!s}' AND dst.name {cmp!s} '{value!s}' RETURN DISTINCT node".format(
            node=self.node, verb=self.verb, value=self.value, cmp='<>' if self.neg else '='
        )
        data = []
        for row in await db.run(query):
            data.append(Node(id=row['node'].id, labels=list(row['node'].labels), properties=dict(zip(row['node'].keys(), row['node'].values()))))
        return data

    def __repr__(self):
        return "{neg}{} {} '{}'".format(self.node, self.verb, self.value, neg="NOT " if self.neg else "")


class Attribute(Relation):
    def __init__(self, node, key, verb, value, neg=False, **kwargs):
        self.key = key
        if not isinstance(self.key, str):
            raise JSONDecodeError("'{}' must be a string".format('key'), key, 0)
        super(Attribute, self).__init__(node, verb, value, neg, **kwargs)

    def __eq__(self, other):
        return super(Attribute, self).__eq__(other) and self.key == other.key

    async def run(self, db):
        await check_node_label(db, self.node)
        await check_node_attribute(db, self.node, self.key)
        assert self.verb in ['=', '!=']
        if self.verb == '!=':
            self.verb = '='
            self.neg = not self.neg
        query = "MATCH (node:{node!s}) WHERE node.{key!s} {cmp!s} '{value!s}' RETURN DISTINCT node".format(
            node=self.node, key=self.key, value=self.value, cmp='<>' if self.neg else '='
        )
        data = []
        for row in await db.run(query):
            data.append(Node(id=row['node'].id, labels=list(row['node'].labels), properties=dict(zip(row['node'].keys(), row['node'].values()))))
        return data

    def __repr__(self):
        return "{}.{} {neg}{} '{}'".format(self.node, self.key, self.verb, self.value, neg="!" if self.neg else "")


class __Logic:
    def __init__(self, *exprs):
        self.exprs = exprs

    def __eq__(self, other):
        return isinstance(other, type(self)) and len(self.exprs) == len(other.exprs) and all([expr in other for expr in self.exprs])

    def __iter__(self):
        return iter(self.exprs)


class Or(__Logic):
    def __repr__(self):
        return "(" + " OR ".join(map(repr, self.exprs)) + ")"
    
    async def run(self, db):
        return list(set.union(*[set(await expr.run(db)) for expr in self.exprs]))


class And(__Logic):
    def __repr__(self):
        return "(" + " AND ".join(map(repr, self.exprs)) + ")"

    async def run(self, db):
        return list(set.intersection(*[set(await expr.run(db)) for expr in self.exprs]))

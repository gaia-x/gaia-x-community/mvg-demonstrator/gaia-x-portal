import json
from .handlers import BaseTest
import tornado.testing
from unittest.mock import Mock, patch
import sys
import http.client
from tornado.httpclient import HTTPError
from server.db import GraphDB
from urllib.parse import urlencode
from server.rules import Attribute, Relation, And, Or
import logging
from .handlers_db import query_to_json


logger = logging.getLogger("tornado.application")
logger.setLevel(logging.DEBUG)


class SelectorTest(BaseTest):
    @tornado.testing.gen_test
    async def test_selector(self):
        response = await self.http_client.fetch(self.reverse_url('ui.selector'))
        objects = json.loads(response.body)
        self.assertIn('selectors', objects)
        self.assertIn({'field': 'Letter', 'options': ['A', 'B', 'C'], 'property': 'name'}, objects['selectors'])


class UISearchTest(BaseTest):
    @tornado.testing.gen_test
    async def test_query_simple_get(self):
        query = Relation("Service", "HOSTED_BY", "provider")    
        logger.info("--> {}".format(query))
        response = await self.http_client.fetch(self.reverse_url('ui.search') + '?' + urlencode({'q': query_to_json(query)}))
        self.assertEqual(response.code, 200)
        objects = json.loads(response.body)
        logger.info("<-- {}".format(objects['query']))
        logger.debug(objects['nodes'])
        self.assertEqual(len(objects['nodes']), 1)
        self.assertIn('provider', objects['nodes'][0]['properties'])
        provider = objects['nodes'][0]['properties']['provider']
        self.assertIn('labels', provider)
        self.assertIn('Provider', provider['labels'])


    @tornado.testing.gen_test
    async def test_query_simple_post(self):
        query = Relation("Service", "HOSTED_BY", "provider")    
        logger.info("--> {}".format(query))
        body = tornado.escape.json_encode({
            'query': json.loads(query_to_json(query))
        })
        headers = tornado.httputil.HTTPHeaders({'content-type': 'application/json'})
        response = await self.http_client.fetch(self.reverse_url('ui.search'), method='POST', body=body, headers=headers)
        self.assertEqual(response.code, 200)
        objects = json.loads(response.body)
        logger.info("<-- {}".format(objects['query']))
        logger.debug(objects['nodes'])
        self.assertEqual(len(objects['nodes']), 1)
        self.assertIn('provider', objects['nodes'][0]['properties'])
        provider = objects['nodes'][0]['properties']['provider']
        self.assertIn('labels', provider)
        self.assertIn('Provider', provider['labels'])

from .handlers import BaseTest
import tornado.testing
from unittest.mock import patch
import subprocess
import tornado.ioloop
import subprocess
import json
from server.rules import expression_decoder, Relation, Attribute, Or, And
import logging
from urllib.parse import urlencode
import http.client
from tornado.httpclient import HTTPError


logger = logging.getLogger("tornado.application")
logger.setLevel(logging.DEBUG)


class NodeJSTest(BaseTest):
    DB_MOCK_VALUES = [
        [
            {'src': 'a', 'rel': '~', 'dst': 'letters'}
        ],[
            {'values': 'a'},
            {'values': 'b'},
            {'values': 'c'}
        ], [
            {'values': '1'},
            {'values': '2'}
        ]
    ]

    async def query(self, query):
        with patch('server.db.GraphDB', autospec=True) as db:
            db().run.side_effect = self.DB_MOCK_VALUES
            url = "http://localhost:{}".format(self._proc_port) + '?' + urlencode({'url': self.reverse_url('db.grammar'), 'query': query})
            response = await self.http_client.fetch(url)
            self.assertEqual(response.code, 200)
            return response.body


class QueryTests(NodeJSTest):
    @classmethod
    def setUpClass(cls):
        cls._proc = subprocess.Popen(["node", "tests/search.js"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if cls._proc.poll() is not None:
            stdout, stderr = cls._proc.communicate()
            logger.warning(stdout.decode())
            raise Exception(stderr.decode())
        line = cls._proc.stdout.readline().decode()
        cls._proc_port = int(line)
        logger.info('NodeJS listening on http://localhost:{}'.format(cls._proc_port))

    @classmethod
    def tearDownClass(cls):
        cls._proc.kill()
        stdout, stderr = cls._proc.communicate()
        logger.info(stdout.decode())
        logger.warning(stderr.decode())

    @tornado.testing.gen_test
    async def test_empty(self):
        with self.assertRaises(HTTPError) as cm:
            await self.query("")
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_property_space(self):
        with self.assertRaises(HTTPError) as cm:
            await self.query("NOT a. 1 = 'anything'")
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_a(self):
        exp = expression_decoder(await self.query("a ~ 'a'"))
        self.assertEqual(exp, Relation("a", "~", "a"))
        exp = expression_decoder(await self.query("NOT NOT a~'a'"))
        self.assertEqual(exp, Relation("a", "~", "a"))
        exp = expression_decoder(await self.query("NOT (NOT a~'a')"))
        self.assertEqual(exp, Relation("a", "~", "a"))

    @tornado.testing.gen_test
    async def test_not_a(self):
        exp = expression_decoder(await self.query("NOT a~'a'"))
        self.assertEqual(exp, Relation("a", "~", "a", neg=True))
        exp = expression_decoder(await self.query("NOT (a~'a')"))
        self.assertEqual(exp, Relation("a", "~", "a", neg=True))

    @tornado.testing.gen_test
    async def test_neq_a_or_b(self):
        exp = expression_decoder(await self.query("NOT a~'a' OR a~'b'"))
        self.assertEqual(exp, Or(Relation("a", "~", "b"), Relation("a", "~", "a", neg=True)))

    @tornado.testing.gen_test
    async def test_not_a_or_b(self):
        exp = expression_decoder(await self.query("NOT (a ~ 'a' OR a ~ 'b')"))
        self.assertEqual(exp, And(Relation("a", "~", "a", neg=True), Relation("a", "~", "b", neg=True)))

    @tornado.testing.gen_test
    async def test_not_a_and_b(self):
        exp = expression_decoder(await self.query("NOT (a ~'a'    AND   a~ 'b')"))
        self.assertEqual(exp, Or(Relation("a", "~", "a", neg=True), Relation("a", "~", "b", neg=True)))

    @tornado.testing.gen_test
    async def test_not_a_and_neq_b(self):
        exp = expression_decoder(await self.query("NOT (a~'a' AND NOT a~'b')"))
        self.assertEqual(exp, Or(Relation("a", "~", "b"), Relation("a", "~", "a", neg=True)))

    @tornado.testing.gen_test
    async def test_a_in_any_b_c(self):
        exp = expression_decoder(await self.query("a ~ ANY('b', 'c')"))
        self.assertEqual(exp, Or(Relation("a", "~", "c"), Relation("a", "~", "b")))

    @tornado.testing.gen_test
    async def test_a_in_all_b_c(self):
        exp = expression_decoder(await self.query("a ~ ALL('b', 'c')"))
        self.assertEqual(exp, And(Relation("a", "~", "c"), Relation("a", "~", "b")))

    @tornado.testing.gen_test
    async def test_a_1_eq_and_a_1_neq(self):
        exp = expression_decoder(await self.query("a.1   = '  ç   | ' AND a.1 != '# ^'"))
        self.assertEqual(exp, And(Attribute("a", "1", "=", "  ç   | "), Attribute("a", "1", "!=", "# ^")))

    @tornado.testing.gen_test
    async def test_a_1_in_any_b_c(self):
        exp = expression_decoder(await self.query("a.1 IN ANY('anything', '@')"))
        self.assertEqual(exp, Or(Attribute("a", "1", "=", "@"), Attribute("a", "1", "=", "anything")))


    @tornado.testing.gen_test
    async def test_not_a_1_in_b_c(self):
        exp = expression_decoder(await self.query("NOT a.1 IN ANY('anything', '@')"))
        self.assertEqual(exp, And(Attribute("a", "1", "=", "@", neg=True), Attribute("a", "1", "=", "anything", neg=True)))

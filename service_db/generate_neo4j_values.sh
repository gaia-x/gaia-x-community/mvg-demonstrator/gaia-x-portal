#!/bin/sh

set -e

# documentation https://neo4j.com/labs/neo4j-helm/1.0.0/configreference/

cat > neo4j_values.yml <<EOF
acceptLicenseAgreement: "yes"
existingPasswordSecret: service-db-neo4j
plugins: "[\"apoc\", \"n10s\"]"
core:
  standalone: true
  persistentVolume:
    size: 10Gi # might be needed later to get more space
EOF

if [ ! ${CI_ENVIRONMENT_NAME} = "portal" ]; then 
  cat >> neo4j_values.yml <<EOF
  service:
    type: NodePort # expose service to Internet. default: ClusterIP for main portal website
EOF
fi

# NEO4J_dbms_jvm_additional
